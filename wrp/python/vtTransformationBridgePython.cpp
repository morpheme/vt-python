// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "BridgePython.h"
#include "xtensor-python/pyarray.hpp"
#include <stdexcept>


using vtTransformation = vtTransformationBridge;


namespace vt::python {
    template<class T>
    /**
     * @brief Copies the contents of a 4x4 Python numpy array to a 4x4 C++ matrix.
     *
     * This function takes a 4x4 numpy array and copies its elements into a
     * 4x4 C++ standard array matrix. If the input array does not have the
     * appropriate dimensions, an error message is logged.
     *
     * @tparam T The data type of the elements in the numpy array.
     * @param array The 4x4 numpy array from which data is to be copied.
     * @param mat A 4x4 C++ standard array matrix where data will be copied.
     */
    void copy_py_array_to_trsf(xt::pyarray<T> &array, std::array<std::array<double, 4>, 4> &mat) {
        std::string proc = "copy_py_array_to_trsf"; // Procedure name for error messages
        auto &&shape = array.shape(); // Retrieve the shape of the input array

        // Ensure shape vector has at least two dimensions
        if (shape.size() != 2) {
            throw std::invalid_argument(
                proc + ": array should be 2D, got " + std::to_string(shape.size()) + "dimensions");
        }
        // Verify that the input array is 4x4
        if (shape[0] != 4 || shape[1] != 4) {
            // Throw an exception if the array is not 4x4
            throw std::invalid_argument(
                proc + ": weird shape values (" + std::to_string(shape[0]) + ", " + std::to_string(shape[1]) + ")");
        }

        // Copy elements from array to mat
        for (std::size_t i = 0; i < 4; i++) {
            for (std::size_t j = 0; j < 4; j++) {
                mat[i][j] = static_cast<double>(array(i, j)); // Access 2D array element and assign to mat
            }
        }
    }

    /**
     * @brief Converts a vtTransformation object to a Python numpy array.
     *
     * This function extracts the 4x4 transformation matrix from a vtTransformation object
     * and populates a Python numpy array with its values.
     *
     * @param trsf A reference to the vtTransformation object containing the matrix to be copied.
     * @return A Python object encapsulating the numpy array with the transformation matrix.
     */
    xt::pyarray<double> copy_trsf_to_py_array(vtTransformation &trsf) {
        std::array<std::array<double, 4>, 4> mat = trsf.matrix(); // Get the 4x4 matrix from vtTransformation
        xt::pyarray<double>::shape_type shape = {4, 4}; // Define the shape for the new pyarray
        xt::pyarray<double> array(shape); // Initialize the pyarray with the defined shape

        // Copy elements from mat to array
        for (std::size_t i = 0; i < 4; ++i) {
            for (std::size_t j = 0; j < 4; ++j) {
                array(i, j) = mat[i][j]; // Assign matrix element to pyarray
            }
        }
        return array; // Return the populated pyarray
    }
}

/**
 * @brief Binds the vtTransformation class and its methods to a Python module.
 *
 * This function exposes the vtTransformation class to Python, including its
 * constructors, methods for setting transformation units, converting arrays,
 * and representing the class as a string. Two constructors are overloaded to
 * accept 2D numpy arrays of both float and double types and initialize
 * vtTransformation objects with the converted matrices.
 *
 * @param m The Python module to which the vtTransformation class is added.
 */
void vtTransformationWrapper(py::module m) {
    // Define the vtTransformation class in the Python module
    py::class_<vtTransformation>(m, "_vtTransformation")

            // Bind the default constructor
            .def(py::init<>(),
                 "Empty object constructor.")

            // Bind the parameterized constructor that accepts a std::string
            .def(py::init<const std::string &>(),
                 "Construct an object using a path to a transformation file.")

            // Overload __init__ to accept a 2D numpy float array and convert it
            .def("__init__", [](vtTransformation &trsf, xt::pyarray<float> &array) {
                     // Create a 4x4 matrix for internal use
                     std::array<std::array<double, 4>, 4> mat{};
                     // Copy data from the numpy array to the internal matrix
                     vt::python::copy_py_array_to_trsf(array, mat);
                     // Initialize the vtTransformation object with the matrix
                     new(&trsf) vtTransformation(mat, "real");
                 },

                 "Construct an object using a transformation matrix with 32-bit values.")
            // Overload __init__ to accept a 2D numpy double array and convert it
            .def("__init__", [](vtTransformation &trsf, xt::pyarray<double> &array) {
                     // Create a 4x4 matrix for internal use
                     std::array<std::array<double, 4>, 4> mat{};
                     // Copy data from the numpy array to the internal matrix
                     vt::python::copy_py_array_to_trsf(array, mat);
                     // Initialize the vtTransformation object with the matrix
                     new(&trsf) vtTransformation(mat, "real");
                 },
                 "Construct an object using a transformation matrix with 64-bit values.")

            // Bind the setUnit method to change the unit of transformation
            .def("setUnit", &vtTransformation::setUnit, py::arg("unit"),
                 "Set the unit of the transformation matrix.")

            // Bind the setUnit method to change the unit of transformation
            .def("unit", &vtTransformation::getUnit,
                 "Get the unit of the transformation matrix.")

            // Overload the __repr__ method to return a string representation
            .def("__repr__", &vtTransformation::toString)

            // Bind the method to copy internal transformation data to a numpy array
            .def("copy_to_array", &vt::python::copy_trsf_to_py_array,
                 "Copy the transformation matrix to an array.")

            // Bind the write method to the Python class
            .def("write", &vtTransformation::write, py::arg("filename"),
                 "Write the transformation matrix to a file.")

            // Bind the print method to the Python class
            .def("print", &vtTransformation::print,
                 "Print the transformation matrix.");
}

//
// vtTransformationBridgePython.cpp ends here
