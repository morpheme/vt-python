// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "BridgePython.h"

#include <xtensor-python/pyarray.hpp>
#include <xtensor-python/pycontainer.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h> // conversions between C++ and Python standard library types

namespace py = pybind11;

using vtImage = vtImageBridge;


namespace vt::python
{
    //
    // FROM py:array_t TO image
    //
    // set the pointer of the py::array_t to the image
    // no data copy from the py::array_t to the image
    //

    template <class T>
    /**
     * @brief Transfer the pointer of a Python array to a vtImage object without data copying.
     *
     * This function links the data buffer of a given xt::pyarray to a vtImage object, adjusting the shape
     * and dimensions of the vtImage based on the shape of the Python array. It supports 2D, 3D, and 3D
     * vectorial images.
     *
     * @param array The xt::pyarray containing the image data.
     * @param img The vtImage object that will be updated with the data and metadata from the Python array.
     */
    void mv_py_array_pointer_to_img(xt::pyarray<T>& array, vtImage& img)
    {
        std::string proc = "mv_py_array_pointer_to_img";
        auto&& shape = array.shape();

        if (shape.size() <= 1)
        {
            std::cerr << proc << ": is not implemented for shape.size() = " << shape.size() << std::endl;
        }
        else if (shape.size() == 2)
        {
            // 2D image
            // recall that the shape of a numpy.ndarray is [[z],y,x,[v]]
            std::array<unsigned long, 3> img_shape = {shape[1], shape[0], 1};
            img.setData(array.data(), false);
            img.setShape(img_shape);
            img.setDims(1);
            img.setType(typeid(T));
        }
        else if (shape.size() == 3)
        {
            // 3D image or 2D vectorial image
            // recall that the shape of a numpy.ndarray is [[z],y,x,[v]]
            // if the third dimension is small, it may be a vectorial image
            if (shape[2] <= 3)
            {
                std::array<unsigned long, 3> img_shape = {shape[1], shape[0], 1};
                img.setData(array.data(), false);
                img.setShape(img_shape);
                img.setDims(shape[2]);
                img.setType(typeid(T));
            }
            else
            {
                std::array<unsigned long, 3> img_shape = {shape[2], shape[1], shape[0]};
                img.setData(array.data(), false);
                img.setShape(img_shape);
                img.setDims(1);
                img.setType(typeid(T));
            }
        }
        else if (shape.size() == 4)
        {
            // 3D vectorial image
            // recall that the shape of a numpy.ndarray is [[z],y,x,[v]]
            std::array<unsigned long, 3> img_shape = {shape[2], shape[1], shape[0]};
            img.setData(array.data(), false);
            img.setShape(img_shape);
            img.setDims(shape[3]);
            img.setType(typeid(T));
        }
        else
        {
            std::cerr << proc << ": is not implemented for shape.size() = " << shape.size() << std::endl;
        }
    }


    //
    // FROM image TO py:array_t
    //

    //
    // copy from the image to the py::array_t
    //

    template <class T>
    /**
     * @brief Copies the data from a vtImage object to a Python array.
     *
     * This function creates a Python array (xt::pyarray) and populates it with data from a given vtImage
     * object. It supports 2D, 3D, and 3D vectorial images by copying the appropriate data buffers from
     * the vtImage to the new Python array.
     *
     * @param img The vtImage object containing the image data to be copied.
     * @return A py::object containing the newly created Python array with the copied image data.
     */
    py::object _copy_img_to_py_array(vtImage& img)
    {
        std::string proc = "mv_img_pointer_to_py_array";
        py::object o;
        std::array<unsigned long, 3> img_shape = img.shape();
        T* data = img.data<T>();

        if (data == nullptr)
        {
            std::cerr << proc << ": data pointer is already NULL " << std::endl;
            return o;
        }

        if (img.dims() > 1)
        {
            // vectorial image
            if (img_shape[2] > 1)
            {
                // 3D vectorial image
                auto arr_shape = std::vector<std::size_t>({img_shape[2], img_shape[1], img_shape[0], img.dims()});
                xt::pyarray<T> array = xt::pyarray<T>(arr_shape);
                T* ptr = array.data();
                std::copy(data, data + array.size(), ptr);
                return array;
            }
            else
            {
                // 2D vectorial image
                auto arr_shape = std::vector<std::size_t>({img_shape[1], img_shape[0], img.dims()});
                xt::pyarray<T> array = xt::pyarray<T>(arr_shape);
                T* ptr = array.data();
                std::copy(data, data + array.size(), ptr);
                return array;
            }
        }
        else
        {
            // scalar image
            if (img_shape[2] > 1)
            {
                // 3D scalar image
                auto arr_shape = std::vector<std::size_t>({img_shape[2], img_shape[1], img_shape[0]});
                xt::pyarray<T> array = xt::pyarray<T>(arr_shape);
                T* ptr = array.data();
                std::copy(data, data + array.size(), ptr);
                return array;
            }
            else
            {
                // 2D scalar image
                auto arr_shape = std::vector<std::size_t>({img_shape[1], img_shape[0]});
                xt::pyarray<T> array = xt::pyarray<T>(arr_shape);
                T* ptr = array.data();
                std::copy(data, data + array.size(), ptr);
                return array;
            }
        }
        return o;
    }

    /**
     * @brief Copies image data from a vtImage object to a Python array.
     *
     * This function determines the data type of the vtImage object and calls the appropriate
     * specialized function to copy the image data into a Python array (py::array_t).
     * It supports multiple data types such as double, unsigned char, char,
     * unsigned short int, short int, unsigned int, int, and float.
     *
     * @param img The vtImage object containing the image data to be copied.
     * @return A py::object containing the newly created Python array with the copied image data.
     */
    py::object copy_img_to_py_array(vtImage& img)
    {
        std::string proc = "copy_img_to_py_array";
        py::object o;

        std::type_index type = img.type();
        if (type == std::type_index(typeid(double)))
        {
            o = _copy_img_to_py_array<double>(img);
        }
        else if (type == std::type_index(typeid(unsigned char)))
        {
            o = _copy_img_to_py_array<unsigned char>(img);
        }
        else if (type == std::type_index(typeid(char)))
        {
            o = _copy_img_to_py_array<char>(img);
        }
        else if (type == std::type_index(typeid(unsigned short int)))
        {
            o = _copy_img_to_py_array<unsigned short>(img);
        }
        else if (type == std::type_index(typeid(short int)))
        {
            o = _copy_img_to_py_array<short>(img);
        }
        else if (type == std::type_index(typeid(unsigned int)))
        {
            o = _copy_img_to_py_array<unsigned int>(img);
        }
        else if (type == std::type_index(typeid(int)))
        {
            o = _copy_img_to_py_array<int>(img);
        }
        else if (type == std::type_index(typeid(float)))
        {
            o = _copy_img_to_py_array<float>(img);
        }
        else
        {
            std::cerr << proc << ": is not implemented for this type " << std::endl;
        }
        return o;
    }


    //
    // set the pointer of the image to the py::array_t
    // no data copy from the image to the py::array_t
    //

    template <class T>
    /**
     * @brief Convert a vtImage object to a Python array without data copying.
     *
     * This function creates a numpy array wrapper around the data buffer of a vtImage object,
     * allowing for zero-copy conversion from a C++ image structure to a Python array. It
     * adjusts the shape and dimensions of the resulting numpy array based on the shape of the vtImage.
     *
     * @param img The vtImage object containing the image data to be converted.
     * @return A Python object representing the numpy array with shared data from the vtImage.
     */
    py::object _mv_img_pointer_to_py_array(vtImage& img)
    {
        std::array<unsigned long, 3> img_shape = img.shape();

        std::intptr_t array_dims[4];
        // recall that the shape of a numpy.ndarray is [[z],y,x,[v]]
        int nd = 0;

        int type_num = xt::detail::numpy_traits<T>::type_num;
        T* data = img.data<T>();
        img.setData((T*)NULL, false);

        if (img.dims() > 1)
        {
            // vectorial image
            if (img_shape[2] > 1)
            {
                // 3D vectorial image
                array_dims[0] = img_shape[2];
                array_dims[1] = img_shape[1];
                array_dims[2] = img_shape[0];
                array_dims[3] = img.dims();
                nd = 4;
            }
            else
            {
                // 2D vectorial image
                array_dims[0] = img_shape[1];
                array_dims[1] = img_shape[0];
                array_dims[2] = img.dims();
                array_dims[3] = 0;
                nd = 3;
            }
        }
        else
        {
            // scalar image
            if (img_shape[2] > 1)
            {
                // 3D scalar image
                array_dims[0] = img_shape[2];
                array_dims[1] = img_shape[1];
                array_dims[2] = img_shape[0];
                array_dims[3] = 0;
                nd = 3;
            }
            else
            {
                // 2D scalar image
                array_dims[0] = img_shape[1];
                array_dims[1] = img_shape[0];
                array_dims[2] = 0;
                array_dims[3] = 0;
                nd = 2;
            }
        }

        // From numpy documentation
        //
        // PyObject* PyArray_SimpleNewFromData(int nd, npy_intp* dims, int typenum, void* data)
        //
        // Create an array wrapper around data pointed to by the given pointer. The array flags will have a
        // default that the data area is well-behaved and C-style contiguous. The shape of the array is given
        // by the dims c-array of length nd. The data-type of the array is indicated by typenum.
        //
        // From /.../miniconda3/envs/.../include/python3.7m/pybind11/pytypes.h
        //
        // Declare that a `handle` or ``PyObject *`` is a certain type and borrow the reference.
        // The target type ``T`` must be `object` or one of its derived classes. The function
        // doesn't do any conversions or checks. It's up to the user to make sure that the
        // target type is correct.
        // Ex:
        // PyObject *p = PyList_GetItem(obj, index);
        // py::object o = reinterpret_borrow<py::object>(p);
        //
        // template <typename T> T reinterpret_borrow(handle h) { return {h, object::borrowed_t{}}; }

        return py::reinterpret_borrow<xt::pyarray<T>>(PyArray_SimpleNewFromData(nd, array_dims, type_num, data));
    }

    /**
     * @brief Converts the data buffer of a vtImage object to a Python array.
     *
     * This function creates a Python object representing the data in a given vtImage object
     * without copying the data. The function checks the type of the data within the vtImage
     * and calls an appropriate template function to handle the conversion for supported types.
     * Supported types include double, unsigned char, char, unsigned short int, short int,
     * unsigned int, int, and float.
     *
     * @param img The vtImage object containing the image data.
     * @return A Python object representing the vtImage data.
     */
    py::object mv_img_pointer_to_py_array(vtImage& img)
    {
        std::string proc = "mv_img_pointer_to_py_array";
        py::object o;

        std::type_index type = img.type();
        if (type == std::type_index(typeid(double)))
        {
            o = _mv_img_pointer_to_py_array<double>(img);
        }
        else if (type == std::type_index(typeid(unsigned char)))
        {
            o = _mv_img_pointer_to_py_array<unsigned char>(img);
        }
        else if (type == std::type_index(typeid(char)))
        {
            o = _mv_img_pointer_to_py_array<char>(img);
        }
        else if (type == std::type_index(typeid(unsigned short int)))
        {
            o = _mv_img_pointer_to_py_array<unsigned short>(img);
        }
        else if (type == std::type_index(typeid(short int)))
        {
            o = _mv_img_pointer_to_py_array<short>(img);
        }
        else if (type == std::type_index(typeid(unsigned int)))
        {
            o = _mv_img_pointer_to_py_array<unsigned int>(img);
        }
        else if (type == std::type_index(typeid(int)))
        {
            o = _mv_img_pointer_to_py_array<int>(img);
        }
        else if (type == std::type_index(typeid(float)))
        {
            o = _mv_img_pointer_to_py_array<float>(img);
        }
        else
        {
            std::cerr << proc << ": is not implemented for this type " << std::endl;
        }

        return o;
    }


    //
    //
    //

    template <class T>
    /**
     * @brief Creates a py::buffer_info object from a vtImage object.
     *
     * This function constructs a py::buffer_info object which describes the buffer
     * of the given vtImage. It determines the shape and strides based on whether
     * the image is scalar or vectorial, and 2D or 3D.
     *
     * @param img The vtImage object from which the buffer information is derived.
     * @return py::buffer_info object containing buffer information for the image.
     */
    py::buffer_info _buffer_info_from_image(vtImage& img)
    {
        py::buffer_info buf;
        std::array<unsigned long, 3> img_shape = img.shape();

        // py::buffer_info parameters:
        //
        // pointer to buffer
        // size of one value
        // Python struct-style format descriptor
        // Number of dimensions
        // Buffer dimensions
        // Strides (in bytes) for each index
        //
        // recall that the shape of a numpy.ndarray is [[z],y,x,[v]]
        // strides are (C order) [see help(numpy.ndarray)]
        // scalar    2D: (dimx, 1)
        // vectorial 2D: (dimx*dimv, dimv, 1)
        // scalar    3D: (dimy*dimx, dimx, 1)
        // vectorial 3D: (dimy*dimx*dimv, dimv, 1)


        if (img.dims() > 1)
        {
            // vectorial image
            if (img_shape[2] > 1)
            {
                // 3D vectorial image
                return py::buffer_info(img.raw_data(), sizeof(T), py::format_descriptor<T>::format(),
                                       4, {img_shape[2], img_shape[1], img_shape[0], img.dims()},
                                       {
                                           sizeof(T) * img_shape[1] * img_shape[0] * img.dims(),
                                           sizeof(T) * img_shape[0] * img.dims(), sizeof(T) * img.dims(), sizeof(T)
                                       });
            }
            else
            {
                // 2D vectorial image
                return py::buffer_info(img.raw_data(), sizeof(T), py::format_descriptor<T>::format(),
                                       3, {img_shape[1], img_shape[0], img.dims()},
                                       {sizeof(T) * img_shape[0] * img.dims(), sizeof(T) * img.dims(), sizeof(T)});
            }
        }
        else
        {
            // scalar image
            if (img_shape[2] > 1)
            {
                // 3D scalar image
                return py::buffer_info(img.raw_data(), sizeof(T), py::format_descriptor<T>::format(),
                                       3, {img_shape[2], img_shape[1], img_shape[0]},
                                       {sizeof(T) * img_shape[1] * img_shape[0], sizeof(T) * img_shape[0], sizeof(T)});
            }
            else
            {
                // 2D scalar image
                return py::buffer_info(img.raw_data(), sizeof(T), py::format_descriptor<T>::format(),
                                       2, {img_shape[1], img_shape[0]},
                                       {sizeof(T) * img_shape[0], sizeof(T)});
            }
        }

        return buf;
    }
}


/**
 * @brief Register the vtImage class with the given Python module, enabling buffer protocol support.
 *
 * This function binds the vtImage class to a Python module using pybind11. It defines several constructors
 * to initialize a vtImage object from various Python array types with optional spacing information. It also
 * exposes member functions for manipulating and accessing image properties and data.
 *
 * @param m Python module where the vtImage class will be registered.
 */
void vtImageWrapper(py::module m)
{
    // Register the vtImage class with the given Python module, enable buffer protocol support.
    py::class_<vtImage>(m, "_vtImage", py::buffer_protocol())

        // No-argument constructor
        .def(py::init<>())
        // Constructor taking a string argument
        .def(py::init<const std::string&>())

        // Constructor taking a Python array of doubles
        .def("__init__", [](vtImage& img, xt::pyarray<double>& array)
        {
            new(&img) vtImage(); // Placement new to initialize the object
            vt::python::mv_py_array_pointer_to_img(array, img); // Move Python array to vtImage data
        })

        // Constructor with Python array of doubles and spacing
        .def("__init__", [](vtImage& img, xt::pyarray<double>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing); // Set the spacing for the image
        })

        // Constructor taking a Python array of floats
        .def("__init__", [](vtImage& img, xt::pyarray<float>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })

        // Constructor with Python array of floats and spacing
        .def("__init__", [](vtImage& img, xt::pyarray<float>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })

        // Repeated for other data types (int, unsigned int, short int, unsigned short int, char, unsigned char)
        .def("__init__", [](vtImage& img, xt::pyarray<int>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<int>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned int>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned int>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<short int>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<short int>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned short int>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned short int>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<char>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<char>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned char>& array)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
        })
        .def("__init__", [](vtImage& img, xt::pyarray<unsigned char>& array,
                            const std::array<float, 3>& spacing)
        {
            new(&img) vtImage();
            vt::python::mv_py_array_pointer_to_img(array, img);
            img.setSpacing(spacing);
        })

        // Define the buffer protocol to expose image data as a buffer
        .def_buffer([](vtImage& img) -> py::buffer_info
        {
            std::string proc = ".def_buffer";
            py::buffer_info buf;
            std::type_index type = img.type(); // Determine the data type of the image

            // Create buffer info based on the type of the image data
            if (type == std::type_index(typeid(double)))
            {
                buf = vt::python::_buffer_info_from_image<double>(img);
                img.setData((void*)nullptr, false); // Reset the image data pointer
                return buf;
            }
            else if (type == std::type_index(typeid(unsigned char)))
            {
                buf = vt::python::_buffer_info_from_image<unsigned char>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(char)))
            {
                buf = vt::python::_buffer_info_from_image<char>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(unsigned short int)))
            {
                buf = vt::python::_buffer_info_from_image<unsigned short>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(short int)))
            {
                buf = vt::python::_buffer_info_from_image<short>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(unsigned int)))
            {
                buf = vt::python::_buffer_info_from_image<unsigned int>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(int)))
            {
                buf = vt::python::_buffer_info_from_image<int>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else if (type == std::type_index(typeid(float)))
            {
                buf = vt::python::_buffer_info_from_image<float>(img);
                img.setData((void*)nullptr, false);
                return buf;
            }
            else
            {
                // If type is not implemented, print an error message
                std::cerr << proc << ": is not implemented for this type " << std::endl;
            }
            return buf;
        })

        // Expose various member functions to Python
        .def("setDims", &vtImage::setDims) // Set the dimensions of the image
        .def("dims", &vtImage::dims) // Get the dimensions of the image
        .def("shape", &vtImage::shape) // Get the shape of the image
        .def("setShape", [](vtImage& img, const std::array<unsigned long, 3>& shape)
        {
            img.setShape(shape); // Set the shape of the image
        })
        .def("spacing", &vtImage::spacing) // Get the spacing of the image
        .def("setSpacing", [](vtImage& img, const std::array<float, 3>& spacing)
        {
            img.setSpacing(spacing); // Set the spacing of the image
        })
        .def("__repr__", [](vtImage& img) -> std::string
        {
            return img.toString(); // Return a string representation of the image
        })
        .def("copy_to_array", vt::python::copy_img_to_py_array) // Copy image to Python array
        .def("to_array", vt::python::mv_img_pointer_to_py_array) // Move image pointer to Python array
        // Overloaded 'write' method to accept pathlib.Path and str
        .def("write", &vtImage::write)
        .def("print", &vtImage::print); // Print image information
}
//
// vtImageBridgePython.cpp ends here
