// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <iostream>

#include "BridgePython.h"
#include <pybind11/numpy.h>
// Define ssize_t for Windows:
#ifdef _WIN32
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

using vtPointList = vtPointListBridge;

namespace vt::python {
    template<class T>
    /**
     * Copies the contents of a Python array into a `vtPointList` object.
     * The input array can have either 2 or 3 columns, representing 2D or 3D points respectively.
     * In case of 2D points, the third coordinate in the `vtPointList` will be set to 0.0.
     *
     * @param array Input Python array of type `xt::pyarray<T>` containing 2D or 3D points.
     * @param pointList Output `vtPointList` object where the points from the array will be copied to.
     */
    void copy_py_array_to_list(py::array_t<T> &array, vtPointList &pointList) {
        // Define a string for procedure name (for error printing)
        const std::string proc = "copy_py_array_to_list";
        auto buf = array.request();
        static_cast<T*>(buf.ptr);
        const auto& shape = buf.shape;

        if (buf.ndim != 2) {
            std::cerr << proc << ": input array must be 2-dimensional, but has " << buf.ndim << " dimensions." << std::endl;
            return;
        }

        size_t rows = shape[0];
        size_t cols = shape[1];
        // Ensure array has enough columns for valid point data
        if (cols != 2 && cols != 3) {
            std::cerr << proc << ": unexpected number of columns=" << cols << ", expected 2 or 3" << std::endl;
            return;
        }
        // Ensure the number of rows is positive
        if (rows <= 0) {
            std::cerr << proc << ": unexpected number of rows=" << rows << ", expected positive value" << std::endl;
            return;
        }

        // Allocate memory in the list to match the number of rows in the input array
        pointList.allocData(shape[0]);
        // Pointer to the data in the list
        auto *data = pointList.data();

        try {
            // Loop over each point and copy data from array to list
            for (std::size_t i = 0; i < shape[0]; ++i) {
                data[i][0] = array.at(i, 0);
                data[i][1] = array.at(i, 1);
                data[i][2] = (shape[1] == 3) ? array.at(i, 2) : 0.0;
            }
        } catch (const std::exception &e) {
            std::cerr << proc << ": exception occurred - " << e.what() << std::endl;
        }
    }

    /**
     * Copies a list of 3D points to a Python array.
     *
     * @param pointList The list of 3D points to be copied. Each point is represented as an array of 3 doubles.
     * @return A Python array containing the 3D points from the input list.
     */
    py::array copy_list_to_py_array(vtPointList &pointList) {
        // Pointer to the data in the list
        auto *data = pointList.data();

        // Define the shape of the resulting Python array (list size x 3 columns)
        std::vector<ssize_t> shape = {static_cast<ssize_t>(pointList.size()), 3};

        // Create a new Python array with the defined shape
        py::array_t<double> array(shape);
        auto buf = array.mutable_unchecked<2>();

        for (ssize_t i = 0; i < pointList.size(); ++i) {
            // Copy each coordinate from the list back into the Python array
            buf(i, 0) = data[i][0];
            buf(i, 1) = data[i][1];
            buf(i, 2) = data[i][2];
        }

        // Return the populated Python array
        return array;
    }
}


/**
 * @brief Wraps the `vtPointList` class for Python using pybind11.
 *
 * This function defines the `_vtPointList` Python class within a given
 * module, exposing various constructors and methods to Python.
 *
 * @param m The Python module in which to define the `_vtPointList` class.
 */
void vtPointListWrapper(py::module m) {
    // Define a new Python class `_vtPointList` in the provided module `m`
    py::class_<vtPointList>(m, "_vtPointList")
            // Bind the default constructor
            .def(py::init<>(),
                 "Empty constructor.")

            // Bind a constructor that takes a `const std::string&` argument
            .def(py::init<const std::string &>(),
                 "Construct an object using a path to a file.")

            // Bind a custom `__init__` method to initialize `vtPointList` from a `py::array_t<float>`
            .def("__init__", [](vtPointList &pointList, py::array_t<float> array) {
                // Placement new to initialize `list` in-place
                new(&pointList) vtPointList();
                // Copy data from the Python array to the `vtPointList` object
                vt::python::copy_py_array_to_list(array, pointList);
            }, "Construct an object using a numpy array with 32-bit values.")

            // Bind a custom `__init__` method to initialize `vtPointList` from a `py::array_t<double>`
            .def("__init__", [](vtPointList &pointList, py::array_t<double> array) {
                // Placement new to initialize `list` in-place
                new(&pointList) vtPointList();
                // Copy data from the Python array to the `vtPointList` object
                vt::python::copy_py_array_to_list(array, pointList);
            }, "Construct an object using a numpy array with 64-bit values.")

            // Bind the method to set the unit of the point list
            .def("setUnit", [](vtPointList &pointList, const std::string &unit) {
                pointList.setUnit(unit);
            })

            // Bind the method to set the spacing with an array of 3 floats
            .def("setSpacing", [](vtPointList &pointList, const std::array<float, 3> spacing) {
                pointList.setSpacing(spacing);
            })

            // Overloaded method to set the spacing with an array of 2 floats
            .def("setSpacing", [](vtPointList &pointList, const std::array<float, 2> spacing) {
                pointList.setSpacing(spacing);
            })

            // Bind the method to get the spacing from the `vtPointListBridge` object
            .def("spacing", &vtPointList::spacing)

            // Bind the method to get the unit from the `vtPointListBridge` object
            .def("unit", &vtPointList::unit)

            // Bind the `__repr__` method to provide a string representation of the object
            .def("__repr__", &vtPointList::toString)

            // Bind the method to copy the list data to a Python array
            .def("copy_to_array", vt::python::copy_list_to_py_array,
                 "Copy the list of points to an array.")

            // Bind the `write` method to write the point list to a file
            .def("write", &vtPointList::write, py::arg("filename"),
                 "Write the list of points to a file.")

            // Bind the `print` method to print the point list to the console
            .def("print", &vtPointList::print,
                 "Print the list of points.");
}

//
// vtPointListBridgePython.cpp ends here
