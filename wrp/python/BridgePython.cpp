#include "BridgePython.h"

#define FORCE_IMPORT_ARRAY
#include "xtensor-python/pyarray.hpp"

void vtCellPropertiesWrapper(py::module m);
void vtImageWrapper(py::module m);
void vtPointListWrapper(py::module m);
void vtTransformationWrapper(py::module m);

PYBIND11_MODULE(vtpython, m)
{
    // Import numpy functionality for xtensor
    xt::import_numpy();

    // Register wrappers for custom types in the module
    vtCellPropertiesWrapper(m);     // Register cell properties wrapper
    vtImageWrapper(m);             // Register image wrapper
    vtPointListWrapper(m);         // Register point list wrapper
    vtTransformationWrapper(m);    // Register transformation wrapper

    // Define functions and corresponding doc functions using pybind11
    m.def("_apply_trsf", vt::API_applyTrsf, "Image resampling");
    m.def("_doc_apply_trsf", vt::DOC_applyTrsf, "Image resampling (help)");

    m.def("_apply_trsf_to_points", vt::API_applyTrsfToPoints, "Point list transformation");
    m.def("_doc_apply_trsf_to_points", vt::DOC_applyTrsfToPoints, "Point list transformation (help)");

    m.def("_blockmatching", vt::API_blockmatching, "Images registration");
    m.def("_doc_blockmatching", vt::DOC_blockmatching, "Images registration (help)");

    m.def("_cellfilter", vt::API_cellfilter, "Cell-based Mathematical morphology");
    m.def("_doc_cellfilter", vt::DOC_cellfilter, "Cell-based Mathematical morphology (help)");

    m.def("_compose_trsf", vt::API_composeTrsf, "Transformations composition");
    m.def("_doc_compose_trsf", vt::DOC_composeTrsf, "Transformations composition (help)");

    m.def("_connexe", vt::API_connexe, "Connected component extraction");
    m.def("_doc_connexe", vt::DOC_connexe, "Connected component extraction (help)");

    m.def("_create_trsf", vt::API_createTrsf, "Transformation creation");
    m.def("_doc_create_trsf", vt::DOC_createTrsf, "Transformation creation (help)");

    m.def("_inv_trsf", vt::API_invTrsf, "Transformation inversion");
    m.def("_doc_inv_trsf", vt::DOC_invTrsf, "Transformation inversion (help)");

    m.def("_linear_filter", vt::API_linearFilter, "Image linear filtering");
    m.def("_doc_linear_filter", vt::DOC_linearFilter, "Image linear filtering (help)");

    m.def("_mean_images", vt::API_meanImages, "Arithmetic operations on several images");
    m.def("_mean_masked_images", vt::API_meanMaskedImages, "Arithmetic operations on several images with associated masks");
    m.def("_doc_mean_images", vt::DOC_meanImages, "Arithmetic operations on several images (help)");

    m.def("_mean_trsfs", vt::API_meanTrsfs, "Arithmetic operations on several transformations");
    m.def("_doc_mean_trsfs", vt::DOC_meanTrsfs, "Arithmetic operations on several transformations (help)");

    m.def("_morpho", vt::API_morpho, "Mathematical morphology");
    m.def("_doc_morpho", vt::DOC_morpho, "Mathematical morphology (help)");

    m.def("_pointmatching", vt::API_pointmatching, "Point lists registration");
    m.def("_doc_pointmatching", vt::DOC_pointmatching, "Point lists registration (help)");

    m.def("_regionalext", vt::API_regionalext, "Regional extrema extraction");
    m.def("_doc_regionalext", vt::DOC_regionalext, "Regional extrema extraction (help)");

    m.def("_watershed", vt::API_watershed, "Seeded watershed segmentation");
    m.def("_doc_watershed", vt::DOC_watershed, "Seeded watershed segmentation (help)");
}