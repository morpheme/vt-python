// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <BridgePython.h>
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

using vtCellProperties = vtCellPropertiesBridge;
using vtImage = vtImageBridge;


namespace vt::python {
    // Function to get labels as a list
    py::list getLabels(vtCellProperties &prop) {
        int n_labels = prop.getLabelNumber(); // Get the number of labels
        // std::cout << "n_labels" << " : " <<  n_labels << std::endl;  // Debugging line
        if (n_labels <= 0)
            return py::none(); // Return `None` if no labels

        int *labels = prop.getLabels(); // Get the labels array
        if (!labels)
            return py::none(); // Return `None` if surfaces is nullptr

        // Create a list from the raw int pointer
        py::list labelsList;
        for (int i = 0; i < n_labels; ++i) {
            labelsList.append(labels[i]);
        }

        delete [] labels; // Free the dynamically allocated labels array
        return labelsList; // Return the list
    }

    // Function to get barycenter of a cell as a 3x1 numpy array
    py::array getBarycenter(vtCellProperties &prop, int currentLabel) {
        std::array<double, 3> vec = prop.getBarycenter(currentLabel); // Get barycenter coordinates
        return py::array(vec.size(), vec.data()); // Return the numpy array
    }

    // Function to get covariance matrix of a cell as a 3x3 numpy array
    py::array getCovariance(vtCellProperties &prop, int currentLabel) {
        std::array<std::array<double, 3>, 3> mat = prop.getCovariance(currentLabel); // Get covariance matrix
        return py::array(mat.size(), mat.data()); // Return the numpy array
    }

    // Function to get eigenvalues of a cell as a 3x1 numpy array
    py::array getEigenvalues(vtCellProperties &prop, int currentLabel) {
        std::array<double, 3> vec = prop.getEigenvalues(currentLabel); // Get eigenvalues
        return py::array(vec.size(), vec.data()); // Return the numpy array
    }

    // Function to get eigenvectors of a cell as a 3x3 numpy array
    py::array getEigenvectors(vtCellProperties &prop, int currentLabel) {
        std::array<std::array<double, 3>, 3> mat = prop.getEigenvectors(currentLabel); // Get eigenvectors
        return py::array(mat.size(), mat.data()); // Return the numpy array
    }

    // Function to get labels of neighbors as a list
    py::list getNeighborLabels(vtCellProperties &prop, int currentLabel) {
        int n_neighbors = prop.getNeighborNumber(currentLabel); // Get the number of neighbors
        if (n_neighbors <= 0)
            return py::none(); // Return `None` if no neighbors

        int *labels = prop.getNeighborLabels(currentLabel); // Get neighbor labels array
        if (!labels)
            return py::none(); // Return `None` if surfaces is nullptr

        // Convert raw pointer to a vector
        std::vector<int> neighborLabels(labels, labels + n_neighbors);
        delete[] labels; // Free the dynamically allocated labels array
        // Convert vector to py::list and return
        return py::cast(neighborLabels);
    }

    // Function to get surfaces of neighbors as a numpy array
    py::array getNeighborsSurfaces(vtCellProperties &prop, int currentLabel) {
        int n_neighbors = prop.getNeighborNumber(currentLabel); // Get the number of neighbors
        if (n_neighbors <= 0)
            return py::none(); // Return `None` if no neighbors

        float *surfaces = prop.getNeighborSurfaces(currentLabel); // Get neighbor surfaces array
        if (!surfaces)
            return py::none(); // Return `None` if surfaces is nullptr

        // Create a numpy array from the raw float pointer
        py::array_t<float> array(n_neighbors, surfaces);
        delete[] surfaces; // Free the dynamically allocated surfaces array
        return array; // Return the numpy array
    }
}

/**
 * @brief Binds the C++ class vtCellProperties to a Python class named "_vtCellProperties".
 *
 * @param m The Python module to which the class is being added.
 */
void vtCellPropertiesWrapper(py::module m) {
    // Binding the C++ class vtCellProperties to a Python class named "_vtCellProperties"
    py::class_<vtCellProperties>(m, "_vtCellProperties")

            // Binding default constructor
            .def(py::init<>(),
                 "Empty constructor.")
            // Binding constructor with a single string argument
            .def(py::init<const std::string &>(), py::arg("path"),
                 "Construct from a path to an XML file or an image.")
            // Binding constructor with only a vtImage reference
            .def(py::init<vtImage &>(), py::arg("image"),
                 "Construct from a vtImage.")
            // Binding constructor with vtImage reference, and filter_neighbors
            .def(py::init<vtImage &, bool>(),
                 py::arg("image"), py::arg("filter_neighbors") = true,
                 "Construct from a vtImage and a boolean to filter labels with neighbors.")
            // Binding constructor with vtImage reference, a vector of strings, and filter_neighbors
            .def(py::init<vtImage &, const std::vector<std::string> &, bool>(),
                 py::arg("image"), py::arg("ppty_list"), py::arg("filter_neighbors") = true,
                 "Construct from a vtImage, a list of properties to compute and a boolean to filter labels with neighbors.")

            // Binding getLabelNumber method which retrieves the number of labels from vtCellProperties
            .def("getLabelNumber", &vtCellProperties::getLabelNumber,
                 "Get the number of labels.")

            // Binding getLabels method which retrieves labels from vtCellProperties
            .def("getLabels", &vt::python::getLabels,
                 "Get a list of labels.")

            // Binding getVolume method which retrieves the volume of a cell given its label
            .def("getVolume", &vtCellProperties::getVolume, py::arg("label"),
                 "Get the volume (number of points) for a given label.")

            // Binding getBarycenter method which retrieves the barycenter of a cell given its label
            .def("getBarycenter", &vt::python::getBarycenter, py::arg("label"),
                 "Get the barycenter of a given label.")

            // Binding getCovariance method which retrieves the covariance matrix of a cell given its label
            .def("getCovariance", &vt::python::getCovariance, py::arg("label"),
                 "Get the covariance matrix of a given label.")

            // Binding getEigenvalues method which retrieves the eigenvalues of a cell given its label
            .def("getEigenvalues", &vt::python::getEigenvalues, py::arg("label"),
                 "Get the eigenvalues of the covariance matrix of a given label.")

            // Binding getEigenvectors method which retrieves the eigenvectors of a cell given its label
            .def("getEigenvectors", &vt::python::getEigenvectors, py::arg("label"),
                 "Get the eigenvectors of the covariance matrix of a given label.")

            /**
             * WARNING:
             * The next `get*` methods require the 'filter_neighbors' argument to be true.
             * They also assume the image to be isotropic!
             */

            // Binding getSurface method which retrieves the surface area of a cell given its label
            .def("getSurface", &vtCellProperties::getSurface, py::arg("label"),
                 "Get the surface area of a given label.")

            // Binding getNeighborNumber method which retrieves number of neighbor labels based on the current label
            .def("getNeighborNumber", &vtCellProperties::getNeighborNumber, py::arg("label"),
                 "Get the number of neighbors of a given label.")

            // Binding getNeighborLabels method which retrieves neighbor labels based on the current label
            .def("getNeighborLabels", &vt::python::getNeighborLabels, py::arg("label"),
                 "Get a list of neighbors of a given label.")

            // Binding getNeighborsSurfaces method which retrieves the surface areas of neighboring cells given a cell label
            .def("getNeighborsSurfaces", &vt::python::getNeighborsSurfaces, py::arg("label"),
                 "Get a list of neighbors' surface areas of a given label.")

            // Binding the write method of vtCellProperties
            .def("write", &vtCellProperties::write, py::arg("path"),
                 "Write an XML file with the computed properties.")

            // Binding the print method of vtCellProperties
            .def("print", &vtCellProperties::print,
                 "Print the computed properties.");
}

//
// vtCellPropertiesBridgePython.cpp ends here
