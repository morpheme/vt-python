#pragma once

#include <pybind11/pybind11.h>   // Pybind11 import to define Python bindings
#include <pybind11/numpy.h>
#include <pybind11/stl.h>       // conversions between C++ and Python standard library types

#include <vtBridge>

namespace py = pybind11;