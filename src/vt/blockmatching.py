#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["blockmatching"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################

#
# blockmatching
# the returned transformation is the computed one,
# it has to be composed with 'trsf_left' (if present) to get the resampling transformation
#

def blockmatching(image_flo, image_ref, trsf_left=None, trsf_init=None, params="", **kwargs):
    """
    Compute a resampling transformation.

    Parameters
    ----------
    image_flo : vtImage or str or Path
        The image to be transformed, or floating image :math:`I_{flo}`.
        Can be a `vtImage` object or the image path.
    image_ref : vtImage or str or Path
        The reference image :math:`I_{ref}`.
        Can be a `vtImage` object or the image path.
    trsf_left : vtTransformation or str or Path, optional
        Left transformation :math:`T_{left}`. The computed transformation is the residual
        right transformation, meaning that the resampling transformation (that allows to resample
        :math:`I_{flo}` in the same frame as :math:`I_{ref}`) is :math:`T_{left} \circ T_{res}`.
        It can be a file path (str or Path) or a vtTransformation object.
    trsf_init : vtTransformation or str or Path, optional
        The initial transformation.
        It can be a file path (str or Path) or a vtTransformation object.
    params : str
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting transformation to a file and return the exit status.

    Returns
    -------
    vtTransformation or int or None
        The resulting transformation :math:`T_{res}` that allows to resample :math:`I_{flo}` in the same frame
         as :math:`I_{ref}`.
        Can be an integer indicating the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image_flo`, `image_ref` and the initial transformation (`trsf_left` or `trsf_init`) are also supposed
     to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import blockmatching
    >>> from vt import vtImage
    >>> from vt.dataset import shared_data_path
    >>> # Load two shared images
    >>> image_flo = vtImage(shared_data_path("flower_confocal", 1))
    >>> image_ref = vtImage(shared_data_path("flower_confocal", 0))
    >>> # Call blockmatching with a pyramid lowest level of 4
    >>> trsf_aff = blockmatching(image_flo, image_ref, params=' -py-ll 4')
    >>> print(trsf_aff.copy_to_array())

    """

    proc = "blockmatching"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(image_flo, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(image_flo)}'for 'image_flo'!")
            return None
        if not isinstance(image_ref, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(image_ref)}'for 'image_ref'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None
        if trsf_left is not None and not isinstance(trsf_left, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf_left)}'for 'trsf_left'!")
            return None
        if trsf_init is not None and not isinstance(trsf_init, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf_init)}'for 'trsf_init'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = ["-floating", image_flo] + ["-reference", image_ref]
        cmd_params += ["-result-transformation", kwargs["out_file_path"]]
        # Add optional transformation parameters if provided
        cmd_params += ["-left-transformation", trsf_left] * (trsf_left is not None)
        cmd_params += ["-initial-result-transformation", trsf_init] * (trsf_init is not None)
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["blockmatching"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(image_flo, vtImage):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if not isinstance(image_ref, vtImage):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(image_ref)}'for 'image_ref'")
            return None
        if trsf_left is not None and not isinstance(trsf_left, vtTransformation):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf_left)}'for 'trsf_left'")
            return None
        if trsf_init is not None and not isinstance(trsf_init, vtTransformation):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf_init)}'for 'trsf_init'")
            return None
        # Call the Python library method
        return vtpython._blockmatching(image_flo, image_ref, trsf_left, trsf_init, params, "")

    # Else, handle unexpected type for `image_flo` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(image_flo)}'for 'image_flo'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(blockmatching, vtpython._doc_blockmatching())
