#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["apply_trsf_to_points"]
_verbose_ = 1

############################################################
#
# classes / structures
#
############################################################

vtPointList = vtpython._vtPointList
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################

def apply_trsf_to_points(point_list, trsf, params="", **kwargs):
    """
    Apply a transformation to a list of point coordinates.

    Parameters
    ----------
    point_list : vtPointList or str or Path
        The list of points to transform.
    trsf : vtTransformation or str or Path
        The transformation to be used.
        This transformation goes from the result image towards the input image.
    params : str
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting transformation to a file and return the exit status.

    Returns
    -------
    vtPointList or int or None
        The transformed list of point coordinates or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `point_list` and `trsf` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import apply_trsf_to_points
    >>> from vt import create_trsf
    >>> from vt import vtPointList
    >>> # Load a shared image
    >>> pts = vtPointList([[0, 0, 1], [1, 2, 3]])
    >>> # Generate a random AFFINE transformation
    >>> trsf = create_trsf(params=" -trsf-type affine -random")
    >>> # Apply the affine transformation
    >>> pts_out = apply_trsf_to_points(pts, trsf)
    >>> print(pts_out.copy_to_array())
    [[ 5.66191744 -0.6126611  -3.14620338]
     [ 8.82613325  0.08714686 -0.67899191]]

    """

    proc = "apply_trsf_to_points"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(point_list, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(point_list)}' for 'point_list'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None
        if trsf is not None and not isinstance(trsf, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf)}' for 'trsf'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = [point_list, kwargs["out_file_path"], "-trsf", trsf]
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["applyTrsfToPoints"] + cmd_params).returncode

    # If 'out_file_path' not in kwargs, execute Python procedure
    elif isinstance(point_list, vtPointList):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")
        # Validate input types for Python procedure
        if trsf is not None and not isinstance(trsf, vtTransformation):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf)}' for 'trsf'!")
            return None
        # Call the Python library method
        return vtpython._apply_trsf_to_points(point_list, trsf, params, "")

    # Else, handle unexpected type for `point_list` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(point_list)}' for 'point_list'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(apply_trsf_to_points, vtpython._doc_apply_trsf_to_points())
