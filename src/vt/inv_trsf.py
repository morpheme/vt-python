#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["inv_trsf"]
_verbose_ = 1

############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################


def inv_trsf(trsf, template_image=None, params="", **kwargs):
    """
    Inverse a transformation.

    Parameters
    ----------
    trsf : vtTransformation
        Transformation to be inverted.
    template_image : vtImage, optional
        For vectorfield transformations, the image frame of the inverse transformation has to be provided.
        This could be either the frame of 'template_image' or the frame of the vectorfield transformation.
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtTransformation or int or None
        The created transformation object or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image`, `trsf` and `ref` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import create_trsf
    >>> from vt import inv_trsf
    >>> # Generate random RIGID transformation:
    >>> trsf = create_trsf(params=" -trsf-type rigid -random")
    >>> trsf.copy_to_array()
    >>> # Invert the transformation:
    >>> i_trsf = inv_trsf(trsf)
    >>> i_trsf.copy_to_array()

    """
    proc = "inv_trsf"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if trsf is not None and not isinstance(trsf, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf)}' for 'trsf'!")
            return None
        if template_image is not None and not isinstance(template_image, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(template_image)}' for 'template_image'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = [trsf, kwargs["out_file_path"]]
        # Add template image if provided
        cmd_params += ["-template", template_image] * (template_image is not None)
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["invTrsf"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(trsf, vtTransformation):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if template_image is not None and not isinstance(template_image, vtImage):
            if _verbose_ >= 1:
                print(str(proc) + ": unexpected type '" + str(type(template_image)) + "'for 'template_image'")
            return None
        # Call the Python library method
        return vtpython._inv_trsf(trsf, template_image, params, "")


    # Else, handle unexpected type for `trsf` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(trsf)}' for 'trsf'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(inv_trsf, vtpython._doc_inv_trsf())
