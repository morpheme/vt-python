#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["pointmatching"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtPointList = vtpython._vtPointList
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################


def pointmatching(points_flo, points_ref, ref=None, filename_residuals=None,
                  params="", **kwargs):
    """
    Computes a transformation with the paired points from the reference to the floating.

    This is somehow the 'blockmatching' way, the resulting transformation will
    allow to resample the floating image (from which the floating points are
    drawn) onto the reference one.
    Points are assumed to be paired according to their rank.

    Parameters
    ----------
    points_flo : ``vtPointList`` object
        The point list to be registered (floating points)
    points_ref : ``vtPointList`` object
        The reference point list (still points)
    ref : ``vtImage`` or ``str``, optional
        Template image for the dimensions of the output transformation
    filename_residuals : str, optional
        Name of the output file for the residual values
    params : str
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtTransformation
        The transformation object resampling the floating points onto the reference points.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `points_flo`, `points_ref`, `ref` & `filename_residuals` are also supposed to be a file name.
    Typically, an exit status of `0` indicates that it ran successfully.

    """
    proc = "pointmatching"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(points_flo, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(points_flo)}' for 'points_flo'!")
            return None
        if not isinstance(points_ref, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(points_ref)}' for 'points_ref'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None
        if ref is not None and not isinstance(ref, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(ref)}' for 'ref'!")
            return None
        if filename_residuals is not None and not isinstance(filename_residuals, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(filename_residuals)}' for 'filename_residuals'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = ["-floating", points_flo] + ["-reference", points_ref]
        cmd_params += ["-result-transformation", kwargs["out_file_path"]]
        # Add reference image if provided
        cmd_params += ["-template", ref] * (ref is not None)
        # Add residuals filename if provided
        cmd_params += ["-residuals", filename_residuals] * (filename_residuals is not None)
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["pointmatching"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(points_flo, vtPointList):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if not isinstance(points_ref, vtPointList):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(points_ref)}' for 'points_ref'!")
            return None
        if ref is not None and not isinstance(ref, vtImage):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(ref)}' for 'ref'!")
            return None
        file_residuals = ""
        if filename_residuals is not None:
            if not isinstance(filename_residuals, str):
                if _verbose_ >= 1:
                    print(f"{proc}: unexpected type '{type(filename_residuals)}' for 'filename_residuals'!")
                return None
            else:
                file_residuals = filename_residuals
        # Call the Python library method
        return vtpython._pointmatching(points_flo, points_ref, ref, file_residuals, params, "")

    # Else, handle unexpected type for `points_flo` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(points_flo)}' for 'points_flo'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(pointmatching, vtpython._doc_pointmatching())
