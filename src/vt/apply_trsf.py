#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Image resampling with a given transformation.
"""

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["apply_trsf"]
_verbose_ = 1

############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################

def apply_trsf(image, trsf=None, ref=None, params="", **kwargs):
    """
    Resample an image with a given transformation.

    Parameters
    ----------
    image : vtImage or str or Path
        The image to be resampled.
        Can be a `vtImage` object or the image path.
    trsf : vtTransformation or str or Path, optional
        The transformation to be used.
        This transformation goes from the result image towards the input image.
    ref : vtImage or str or Path, optional
        A template image, used to define the geometry of the result image (dimension, voxel size).
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtImage or int or None
        The transformed `vtImage` or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image`, `trsf` and `ref` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import apply_trsf
    >>> from vt import create_trsf
    >>> from vt import vtImage
    >>> from vt.dataset import shared_data_path
    >>> # Load a shared image
    >>> image = vtImage(shared_data_path("flower_confocal", 0))
    >>> # Generate a random AFFINE transformation
    >>> trsf = create_trsf(params=" -trsf-type affine -random")
    >>> # Apply the affine transformation
    >>> resample_image = apply_trsf(image, trsf)
    >>> print(resample_image.shape())
    [460, 460, 320]

    """
    proc = "apply_trsf"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(image, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(image)}' for 'image'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None
        if trsf is not None and not isinstance(trsf, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf)}' for 'trsf'!")
            return None
        if ref is not None and not isinstance(ref, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(ref)}' for 'ref'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = [image, kwargs["out_file_path"]]
        # Add transformation if provided
        cmd_params += ["-trsf", trsf] * (trsf is not None)
        # Add reference image if provided
        cmd_params += ["-ref", ref] * (ref is not None)
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["applyTrsf"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(image, vtImage):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if trsf is not None and not isinstance(trsf, vtTransformation):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsf)}' for 'trsf'!")
            return None
        if ref is not None and not isinstance(ref, vtImage):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(ref)}' for 'ref'!")
            return None
        # Call the Python library method
        return vtpython._apply_trsf(image, trsf, ref, params, "")

    # Else, handle unexpected type for `image` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(image)}' for 'image'")
    # Return `None` for unhandled types or errors
    return None

#
#
#

tools.add_doc(apply_trsf, vtpython._doc_apply_trsf())
