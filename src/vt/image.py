#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import logging
from pathlib import Path

import numpy as np

from . import vtpython

__all__ = ["Image"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtCellProperties = vtpython._vtCellProperties

#: Create a logger
log = logging.getLogger(__name__)

#: List of valid data types, _i.e._ accepted by `vt.vtImage`:
VALID_DTYPES = ["uint8", "uint16", "uint32", "float32", "float64"]


def is_valid_dtype(arr):
    """Check if given array has a valid dtype.

    Parameters
    ----------
    arr : numpy.ndarray
        Array to check.

    Returns
    -------
    bool
        Indicate if the array has a valid dtype.
    """
    return arr.dtype.name in VALID_DTYPES


def log_dtype_warning(arr, logger):
    """Log a warning if the dtype of the image is not valid.

    Parameters
    ----------
    arr : numpy.ndarray
        The array to be validated for dtype.
    logger : logging.Logger
        Logger object used to log warnings if the dtype is invalid.
    """
    if not is_valid_dtype(arr):
        logger.warning(f"Given `dtype` ({arr.dtype.name}) is not a valid `dtype`: {', '.join(VALID_DTYPES)}!")
        logger.warning("Use at your own risks...")
    return


def _array_spacing(vtimg):
    """Get the image array and voxel-size.

    Parameters
    ----------
    vtimg : vt.vtImage or vt.Image
        The image to get array and voxel-size from.

    Returns
    -------
    numpy.ndarray
        The image array, ZYX sorted.
    numpy.ndarray
        The voxelsize array, ZYX sorted.
    """
    vxs = vtimg.spacing()
    if isinstance(vtimg, vtImage):
        vxs = vxs[::-1]
    if len(vtimg.shape()) == 2:
        vxs = vxs + [1.]
    return vtimg.copy_to_array(), vxs


class Image(vtImage):
    """Simple wrapper to ``vt.vtImage``.

    Examples
    --------
    >>> from vt.image import Image
    >>> from vt.tools import random_array
    >>> # EXAMPLE #1 - Initialize an EMPTY `vtImage` object:
    >>> im = Image()
    >>> # EXAMPLE #2 - Initialize from an array and a list of voxelsize:
    >>> arr = random_array([3, 4, 5], dtype='uint8')
    >>> arr.shape
    [3, 4, 5]
    >>> im = Image(arr, [0.31, 0.32, 0.33])
    >>> im.ndim()  # access number of dimensions in image
    3
    >>> im.shape()  # access image shape
    [30, 40, 50]
    >>> im.spacing()  # access image voxelsize
    [0.3100000023841858, 0.3199999928474426, 0.33000001311302185]
    >>> im.copy_to_array()  # return a copy of the numpy array
    >>> # EXAMPLE #3 - Initialize from an array with invalid dtype:
    >>> arr = random_array([3, 4, 5], dtype='int16')
    >>> im = Image(arr, [0.31, 0.32, 0.33])
    Given `dtype` (int16) is not a valid `dtype`: uint8, uint16, uint32, float32, float64!
    Use at your own risks...
    >>> im.copy_to_array()  # return a copy of the numpy array
    >>> # EXAMPLE #4 - Initialize from an array and a list of voxelsize:
    >>> arr = random_array([3, 4], dtype='uint8')
    >>> arr.shape
    [3, 4]
    >>> im = Image(arr, [0.31, 0.32])
    >>> im.ndim()  # access number of dimensions in image
    2
    >>> im.shape()  # access image shape
    [30, 40, 50]
    >>> im.spacing()  # access image voxelsize
    [0.3100000023841858, 0.3199999928474426, 0.33000001311302185]
    """

    def __init__(self, image=None, voxelsize=None, **kwargs):
        """Constructor.

        Parameters
        ----------
        image : str or numpy.ndarray, optional
            If a `str` is given, it should be a valid image file to open.
            If a `numpy.ndarray`, it is considered as (Z)YX ordered.
            If `None`, an empty object is built.
        voxelsize : list of float, optional
            Specify the pixel size (if 2D) or voxel size (if 3D), only used if `image` is also specified.
            If specified, should be a (planes,) rows & columns or (Z)YX sorted
            list of voxelsize values, else it will be set to [(1.,) 1., 1.].

        Notes
        -----
        Accepted type of numpy array are: {"uint8", "uint16", "uint32", "float32", "float64"}.

        Notable modifications to `vt.vtImage` are:

         - renaming `setShape` method to `set_shape`;
         - renaming `setSpacing` method to `set_spacing`;
         - fixed mandatory XYZ spacing to (Z)YX (init and setter);
         - fixed XY(Z) shape to (Z)YX;

        Raises
        ------
        TypeError
            If incompatible constructor arguments.
        """
        if image is None:
            # Initializing an empty `vtImage` object if no image was provided
            log.debug('Initializing empty `vtImage` object...')
            super().__init__()
        elif isinstance(image, str):
            # Loading an image from a file path given as a string
            log.debug(f"Loading image file: {image}...")
            super().__init__(image)
        elif isinstance(image, Path):
            # Loading an image from a file path given as a Path object, converting Path to string
            log.debug(f"Loading image file: {image}...")
            super().__init__(str(image))
        elif isinstance(image, vtImage):
            # Extracting array and voxelsize from the provided vtImage object
            array, voxelsize = _array_spacing(image)
            log.debug(f"Initializing from a {array.ndim}D vt.vtImage...")
            log_dtype_warning(array, log)  # Log a warning if the data type of the image is not ideal
            super().__init__(array, voxelsize)  # Initialize the vtImage with both image data and voxel size
        elif isinstance(image, np.ndarray):
            # Determine the number of dimensions (2D or 3D) from the numpy array
            ndim = 2 if len(image.shape) == 2 else 3
            log.debug(f"Initializing from a {ndim}D array...")
            log_dtype_warning(image, log)  # Log a warning if the data type of the image is not ideal
            # Setup voxelsize if it is provided
            if voxelsize is not None:
                voxelsize = voxelsize[::-1]  # Reverse order from (Z)YX to XY(Z) as required by vtImage
                if ndim == 2 and len(voxelsize) == 2:
                    voxelsize = voxelsize + [1.0]  # Ensure voxelsize array is length-3
            else:
                # Attempt to get a 'voxelsize' attribute from the image for compatibility
                voxelsize = getattr(image, "voxelsize", None)
            # Proceed with initialization using the voxelsize if available
            if voxelsize:
                super().__init__(image, voxelsize)
            else:
                super().__init__(image)  # Initialize without voxelsize if not available
        else:
            # Raise an error if the provided image type is unknown and cannot be handled
            raise TypeError(f"Unknown `image` type: '{type(image)}'!")

    # --------------------------------------------------------------------------
    #
    # vtImage IO
    #
    # --------------------------------------------------------------------------

    def to_array(self):
        """Return the image array as a `numpy.ndarray` and destroy the object.

        See Also
        --------
        self.to_array(): returns the image array as a (Z)YX sorted `numpy.ndarray`.

        Returns
        -------
        numpy.ndarray
            The image array

        """
        # - `vt.vtImage.to_array()` methods returns (Z)YX sorted arrays
        return vtImage.to_array(self)

    def copy_to_array(self):
        """Return the image array as a `numpy.ndarray`.

        See Also
        --------
        self.to_array(): returns the image array as a (Z)YX sorted `numpy.ndarray`.

        Returns
        -------
        numpy.ndarray
            The image array

        """
        # - `vt.vtImage.copy_to_array()` methods returns (Z)YX sorted arrays
        return vtImage.copy_to_array(self)

    # --------------------------------------------------------------------------
    #
    # vtImage IO
    #
    # --------------------------------------------------------------------------

    def write(self, filename):
        """Save the image to the disk.

        Parameters
        ----------
        filename : str
            Name of the file to save.

        """
        vtImage.write(self, str(filename))
        log.info(f"Saved {filename}!")

    # --------------------------------------------------------------------------
    #
    # vtImage properties - GETTERS
    #
    # --------------------------------------------------------------------------

    def shape(self):
        """Return the image shape.

        Returns
        -------
        list of int
            (Z)YX sorted shape values.

        """
        # - `vt.vtImage.shape()` method returns an XYZ list of values:
        vt_shape = vtImage.shape(self)
        if 1 in vt_shape:
            return vtImage.shape(self)[:-1][::-1]  # convert to YX
        else:
            return vtImage.shape(self)[::-1]  # convert to ZYX

    def spacing(self):
        """Return the image voxelsize.

        Returns
        -------
        list of float
            (Z)YX sorted voxelsize values.

        """
        # - `vt.vtImage.voxelsize()` method ALWAYS returns a LENGTH-3 XYZ list of values:
        if self.ndim() == 2:
            return vtImage.spacing(self)[:-1][::-1]  # convert to YX
        else:
            return vtImage.spacing(self)[::-1]  # convert to ZYX

    def ndim(self):
        """
        Returns the number of dimensions of the array.

        Returns
        -------
        int
            The number of dimensions.
        """
        return len(self.shape())

    def v_dim(self):
        """Return the number of vector components in image.

        If ``1``, it is a regular image.
        If ``3``, it is a non-liner transformation matrix.

        Returns
        -------
        int
            The number of vector components in the image.
        """
        return self.dims()

    # --------------------------------------------------------------------------
    #
    # vtImage properties - SETTERS
    #
    # --------------------------------------------------------------------------

    def set_spacing(self, spacing):
        """Set the image voxelsize.

        Parameters
        ----------
        spacing : list of int
            A (Z)YX list of voxelsize.

        Notes
        -----
        This changes the image physical size!

        Examples
        --------
        >>> from vt.image import Image
        >>> from vt.tools import random_array
        >>> # - Initialize from generated random numpy array:
        >>> arr = random_array((30, 40, 50), dtype='uint8')
        >>> im = vtImage(arr)
        >>> im.spacing()
        [1., 1., 1.]
        >>> im.set_spacing([0.31, 0.62, 0.63])
        >>> im.spacing()
        [0.3100000023841858, 0.6200000047683716, 0.6299999952316284]

        """
        # - `vtImage.setSpacing()` method expects a LENGTH-3 XYZ list of values:
        spacing = spacing[::-1]
        if len(self.shape()) == 2:
            spacing = spacing + [1.]
        self.setSpacing(spacing)
