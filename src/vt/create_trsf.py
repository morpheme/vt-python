#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["create_trsf"]
_verbose_ = 1

############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################


def create_trsf(template_image=None, fixedpoint_image=None, params="", **kwargs):
    """
    Create a transformation object.

    Parameters
    ----------
    template_image : vtImage or str or Path, optional
        For vectorfield transformations, the image frame of the transformation
        has to be given. It is the one of the 'template_image' image, or it has to be given
        in the 'params' field.
    fixedpoint_image : vtImage or str or Path, optional
        The image center is a fixed point for the computed (e.g., random) transformation.
        It allows ensuring that the (random) transformation will not transform the
        image too far away. It sets the translation component, which is no more random.
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtTransformation or int or None
        The created transformation object or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image`, `trsf` and `ref` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import create_trsf
    >>> # Generate IDENTITY transformation:
    >>> id_trsf = create_trsf(params=' -transformation-value identity -trsf-type affine')
    >>> id_trsf.print()
    >>> # Generate random RIGID transformation:
    >>> trsf = create_trsf(params=" -trsf-type rigid -random")
    >>> trsf.print()
    >>> # Generate random AFFINE transformation:
    >>> trsf = create_trsf(params=" -trsf-type affine -random")
    >>> trsf.print()
    >>> # Generate random VECTORFIELD transformation:
    >>> trsf = create_trsf(params=" -trsf-type vectorfield -value sinus3D -template-dim 30 30 10 -voxel-size 0.2 0.2 0.5")
    >>> trsf.print()

    """
    proc = "create_trsf"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if template_image is not None and not isinstance(template_image, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(template_image)}' for 'template_image'")
            return None
        if fixedpoint_image is not None and not isinstance(fixedpoint_image, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(fixedpoint_image)}' for 'fixedpoint_image'")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = [kwargs["out_file_path"]]
        if template_image is not None:
            cmd_params += ["-template", template_image]
        if fixedpoint_image is not None:
            cmd_params += ["-template-fixedpoint", fixedpoint_image]
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["createTrsf"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    else:
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if template_image is not None and not isinstance(template_image, vtImage):
            if _verbose_ >= 1:
                print(str(proc) + ": unexpected type '" + str(type(template_image)) + "'for 'template_image'")
            return None
        if fixedpoint_image is not None and not isinstance(fixedpoint_image, vtImage):
            if _verbose_ >= 1:
                print(str(proc) + ": unexpected type '" + str(type(fixedpoint_image)) + "'for 'fixedpoint_image'")
            return None

        # Call the Python library method
        return vtpython._create_trsf(template_image, fixedpoint_image, params, "")


#
#
#

tools.add_doc(create_trsf, vtpython._doc_create_trsf())
