#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["compose_trsf"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtTransformation = vtpython._vtTransformation


############################################################
#
# Wrapped APIs
#
############################################################

def compose_trsf(trsfs, params="", **kwargs):
    """
    Composes a list of transformations into a single transformation.

    Parameters
    ----------
    trsfs : list of vtTransformation or list of str or list of Path
        List of transformations to be composed.
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtTransformation or int or None
        A composed transformation object or the exit status if `out_file_path` is defined
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `trsfs` is also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import create_trsf
    >>> from vt import compose_trsf
    >>> # Generate a random AFFINE transformation
    >>> trsf_1 = create_trsf(params=" -trsf-type affine -random")
    >>> trsf_2 = create_trsf(params=" -trsf-type affine -random")
    >>> out_trsf = compose_trsf([trsf_1, trsf_2])
    >>> print(out_trsf.copy_to_array())

    """
    proc = "compose_trsf"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(trsfs, list) or not isinstance(trsfs[0], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsfs)}' for 'trsfs' or '{type(trsfs[0])}' for 'trsfs[0]'")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'kwargs[\"out_file_path\"]'")
            return None

        # Build command parameters for subprocess call
        cmd_params = ["-trsfs"] + trsfs + ["-res", kwargs["out_file_path"]]
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["composeTrsf"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(trsfs, list):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if not isinstance(trsfs[0], vtTransformation):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(trsfs[0])}' for 'trsfs[0]'")
            return None

        # Call the Python library method
        return vtpython._compose_trsf(trsfs, "", params)

    # Else, handle unexpected type for `trsfs` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(trsfs)}' for 'trsfs'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(compose_trsf, vtpython._doc_compose_trsf())
