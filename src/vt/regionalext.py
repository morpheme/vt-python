#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["regionalext"]
_verbose_ = 1

############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage


############################################################
#
# Wrapped APIs
#
############################################################


def regionalext(image, params="", **kwargs):
    """
    Regional extrema detection.

    Parameters
    ----------
    image : vtImage or str or Path
        The image to be used for regional extrema detection.
        Can be a `vtImage` object or the image path.
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtImage or int or None
        The transformed `vtImage` or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image`, `trsf` and `ref` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    Examples
    --------
    >>> from vt import regionalext
    >>> from vt import vtImage
    >>> from vt.dataset import shared_data_path
    >>> # Load a shared image
    >>> image = vtImage(shared_data_path("flower_confocal", 0))
    >>> h_min = 5
    >>> hmin_image = regionalext(image, params=f"-minima -connectivity 26 -h {h_min}")

    """
    # default_params = "-minima -connectivity 26 -h 5 -diff "
    proc = "regionalext"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(image, (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(image)}' for 'image'!")
            return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = [image, kwargs["out_file_path"]]
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["regionalext"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(image, vtImage):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Call the Python library method
        return vtpython._regionalext(image, params, "")

    # Else, handle unexpected type for `image` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(image)}' for 'image'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(regionalext, vtpython._doc_regionalext())
