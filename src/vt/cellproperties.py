#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from collections.abc import Iterable

from . import vtpython

__all__ = ["CellProperties"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage
vtCellProperties = vtpython._vtCellProperties

############################################################
#
# Wrapped APIs
#
############################################################

#
# morpho
#

DEF_PPTY_LIST = ["volume", "surface", "barycenter", "covariance", "contact-surface"]


class CellProperties(object):
    """
    Class to get 'cell' properties.

    Attributes
    ----------
    properties : vtCellProperties
        The wrapped cell properties object from `vt`.
    ppty_list : list
        The list of cell properties to compute.

    Notes
    -----
    'Cells' are labeled regions in an 3D image.

    Examples
    --------
    >>> from vt.dataset import shared_data
    >>> from vt.cellproperties import CellProperties
    >>> im = shared_data('flower_labelled', 0)
    >>> ppty = CellProperties(im)
    >>> print(ppty.labels()[:5])
    [1, 2, 3, 4, 5]
    >>> print(len(ppty.labels()))
    1040
    >>> print(ppty.neighbor(4))
    [  1 445 241 158 998 337 390 142   5 901  31 153]
    >>> print(ppty.neighbor_number(4))
    12
    >>> print(ppty.volume(4))
    24373
    >>> print(ppty.surface(4))
    4767.4716796875
    >>> print(ppty.barycenter(4))
    [160.79021868 196.17154228 104.64940713]
    >>> print(ppty.covariance(4))
    [[6.31624338e+08 7.68835577e+08 4.09381750e+08]
     [7.68835577e+08 9.39184585e+08 5.00376452e+08]
     [4.09381750e+08 5.00376452e+08 2.69721620e+08]]
    >>> print(ppty.contactsurface(4)[1])
    1245.115234375

    >>> ppty = CellProperties(im, ['volume'], filter_neighbors=False)
    >>> print(ppty.properties.getLabelNumber())
    1042
    >>> print(ppty.volume(4))
    24373
    >>> print(ppty.surface(4))
    4767.4716796875

    """

    def __init__(self, image=None, ppty_list=None, filter_neighbors=True):
        """
        Parameters
        ----------
        image : str or vtImage
            A parameter for the initialization of the class instance.
            It can be:
            * a string designated a file name that can be either a xml file or an image file
              (in the former, properties are read from the file, while they are recomputed in the latter case)
            * a vtImage: properties are then recomputed
        ppty_list : list, optional
            The list of cell properties to compute.
            Use this parameter only to restrict the amount of pre-computation, some methods may thus return `0` values.
        filter_neighbors : bool, optional
            Filter the labels to retain only those with at least one neighbor.
            Default to `False`.
        """
        if ppty_list is None:
            self.ppty_list = DEF_PPTY_LIST
        else:
            self.ppty_list = ppty_list

        if isinstance(image, str) or isinstance(image, vtImage):
            self.properties = vtCellProperties(image, self.ppty_list, filter_neighbors)
        else:
            self.properties = None

    def n_labels(self):
        """
        Retrieve the number of labeled regions.

        Returns
        -------
        int
            The number of labels in the labelled image.
        """
        return self.properties.getLabelNumber()

    def labels(self):
        """
        Retrieve the list of labeled regions.

        Returns
        -------
        list
            A list of labels, or ``None`` if there are no labels.
        """
        return self.properties.getLabels()

    def _label_property(self, label, method_name):
        """
        Abstract static method to return label based properties from ``vtCellProperties`` object.

        Parameters
        ----------
        label : int or iterable or None
            A label or a collection of labels for which the property should be returned.
            If ``None``, properties for all labels are returned.
        method_name : str
            The name of the method used to access the label property.
            If its starts with 'properties', it should be the name of a ``vtCellProperties`` method.
            It should accept a label and return the computed property.

        Returns
        -------
        any
            If a single label is given, return the property value for that label.
            Else, a label indexed dictionary of property values.
        """
        property = {}  # default property value is an empty dict
        labels = self.labels()
        # Return an empty dictionary if 'labels' is None or empty
        if labels is None or len(labels) == 0:
            return property

        # Get the method to access the label property
        if method_name.startswith("properties"):
            # Access a `vtCellProperties` method
            self_attr, method_name = method_name.split('.')
            ppty_method = getattr(self.properties, method_name)
        else:
            # Access a `CellProperties` method
            ppty_method = getattr(self, method_name)

        # If 'label' is None, return property values for all 'labels'
        if label is None:
            for i in labels:
                property[i] = ppty_method(i)
        # If 'label' is an integer, return property for the specified label
        elif isinstance(label, int):
            property = ppty_method(label)
        # If 'label' is an iterable, return property values for common labels
        elif isinstance(label, Iterable):
            in_labels = set(label) & set(labels)
            for i in in_labels:
                property[i] = ppty_method(i)
        # Raise TypeError if 'label' is not an acceptable type
        else:
            raise TypeError(f"Parameter `label` must be an int, an iterable, or None, got '{type(label)}' instead.'!")
        return property

    def volume(self, label=None):
        """
        Retrieve the volume of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of volume for all labels (default).
            If an integer, returns the volume for that specific label.
            If an iterable, returns the dictionary of volume for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' volumes (in voxel units).
        """
        return self._label_property(label, 'properties.getVolume')

    def barycenter(self, label=None):
        """
        Retrieve the barycenter of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of barycenter for all labels (default).
            If an integer, returns the barycenter for that specific label.
            If an iterable, returns the dictionary of barycenter for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' barycenter (in voxel units).
        """
        return self._label_property(label, 'properties.getBarycenter')

    def covariance(self, label=None):
        """
        Retrieve the covariance matrix of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of covariance matrix for all labels (default).
            If an integer, returns the covariance matrix for that specific label.
            If an iterable, returns the dictionary of covariance matrix for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' covariance matrix (in voxel units).
        """
        return self._label_property(label, 'properties.getCovariance')

    def eigenvalue(self, label=None):
        """
        Retrieve the covariance matrix eigenvalue of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of covariance matrix eigenvalue for all labels (default).
            If an integer, returns the covariance matrix eigenvalue for that specific label.
            If an iterable, returns the dictionary of covariance matrix eigenvalue for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' covariance matrix eigenvalue (in voxel units).
        """
        return self._label_property(label, 'properties.getEigenvalues')

    def eigenvector(self, label=None):
        """
        Retrieve the covariance matrix eigenvector of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of covariance matrix eigenvector for all labels (default).
            If an integer, returns the covariance matrix eigenvector for that specific label.
            If an iterable, returns the dictionary of covariance matrix eigenvector for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' covariance matrix eigenvector (in voxel units).
        """
        return self._label_property(label, 'properties.getEigenvectors')

    def surface(self, label=None):
        """
        Retrieve the surface of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of surface for all labels (default).
            If an integer, returns the surface for that specific label.
            If an iterable, returns the dictionary of surface for the given list of labels.

        Returns
        -------
        property : dict
            A label-indexed dictionary of cells' surfaces (in voxel units).

        Notes
        -----
        Voxels are assumed to be isotropic, the image must be resampled prio to instantiation if not.
        This requires `filter_neighbors=True` at instantiation!
        """
        return self._label_property(label, 'properties.getSurface')

    def neighbor_number(self, label=None):
        """
        Retrieve the number of neighbors of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of number of neighbors for all labels (default).
            If an integer, returns the number of neighbors for that specific label.
            If an iterable, returns the dictionary of number of neighbors for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' number of neighbors.

        Notes
        -----
        This requires `filter_neighbors=True` at instantiation!
        """
        return self._label_property(label, 'properties.getNeighborNumber')

    def neighbor(self, label=None):
        """
        Retrieve the neighbors of labeled regions.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of neighbors for all labels (default).
            If an integer, returns the neighbors for that specific label.
            If an iterable, returns the dictionary of neighbors for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' number of neighbors.

        Notes
        -----
        This requires `filter_neighbors=True` at instantiation!
        """
        return self._label_property(label, 'properties.getNeighborLabels')

    def _get_neighbor_surfaces(self, label):
        """
        Retrieve the neighbor surfaces for a given label.

        Parameters
        ----------
        label : int
            The label to get the neighbor surfaces for.

        Parameters
        ----------
        label : int
            The identifier for the label whose neighboring surfaces are to be retrieved.

        Returns
        -------
        dict
            A dictionary where keys are neighboring label identifiers and values are the corresponding surface areas.
            If no neighbors found, returns an empty dictionary.
        """
        nei_surf = {}  # Initialize an empty dictionary to store neighbor surface areas
        # Get the list of neighboring labels for the given label
        neighbors = self.properties.getNeighborLabels(label)
        # Return empty dictionary if no neighbors are found
        if neighbors is None:
            return nei_surf
        # Get the corresponding surface areas for the neighbors
        surfaces = self.properties.getNeighborsSurfaces(label)
        # Iterate through the neighbors and populate the dictionary with neighbor labels and their surface areas
        for nei_idx in range(len(neighbors)):
            nei_surf[int(neighbors[nei_idx])] = float(surfaces[nei_idx])
        return nei_surf

    def contactsurface(self, label=None):
        """
        Retrieve the contact surface of labeled regions with their neighbors.

        Parameters
        ----------
        label : int, Iterable, or None, optional
            If ``None``, returns the dictionary of contact surface for all labels (default).
            If an integer, returns the contact surface for that specific label.
            If an iterable, returns the dictionary of contact surface for the given list of labels.

        Returns
        -------
        dict
            A label-indexed dictionary of cells' contact surface with their neighbors (in voxel units).

        Notes
        -----
        Voxels are assumed to be isotropic, the image must be resampled prio to instantiation if not.
        This requires `filter_neighbors=True` at instantiation!
        """
        return self._label_property(label, '_get_neighbor_surfaces')

    def write(self, filename):
        """
        Save the cell properties into an XML file.

        Parameters
        ----------
        filename : str
            The name of the XML file.
        """
        self.properties.write(filename)

    def print(self):
        """Prints the `properties` attribute of the object."""
        self.properties.print()

#
#
#
