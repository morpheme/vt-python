#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from pathlib import Path

from . import tools
from . import vtpython

__all__ = ["mean_images"]
_verbose_ = 1
############################################################
#
# classes / structures
#
############################################################

vtImage = vtpython._vtImage


############################################################
#
# Wrapped APIs
#
############################################################


def mean_images(images, masks=None, params="", **kwargs):
    """
    Image averaging.

    Parameters
    ----------
    image : list of vtImage or list of str or list of Path
        The images to be averaged.
        Can be `vtImage` objects or the image paths.
    masks :  : list of vtImage or list of str or list of Path
        The masks to apply during averaging.
        Can be `vtImage` objects or the mask paths.
    params : str, optional
        A string containing computation options (from inline C API).
        Default is an empty string.

    Other Parameters
    ----------------
    out_file_path : str or Path, optional
        Write the resulting image to a file and return the exit status.

    Returns
    -------
    vtImage or int or None
        The averaged `vtImage` or the exit status if `out_file_path` is defined.
        Can be `None` if a type error is encountered with an input parameter.

    Notes
    -----
    If `out_file_path` is found in the additional variables, this will call a sub-process with the inline command.
    Hence, `image`, `trsf` and `ref` are also supposed to be file names.
    Typically, an exit status of `0` indicates that it ran successfully.

    """
    proc = "mean_images"  # Process/function identifier
    # Verify if params is provided and is of type str
    if params is not None and not isinstance(params, str):
        if _verbose_ >= 1:  # Conditional print based on verbosity level
            print(f"{proc}: unexpected type '{type(params)}' for 'params'!")
        return None

    # Check if `out_file_path` is specified in kwargs, indicating an inline command call
    if "out_file_path" in kwargs:
        if _verbose_ >= 2:
            print(f"call of '{proc}': inline command")

        # Validate input types for inline command call
        if not isinstance(images, list) and not isinstance(images[0], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(images)}' for 'images' or")
                print(f"{proc}: unexpected type '{type(images[0])}' for 'images[0]'")
            return None
        if masks is not None:
            if not isinstance(masks, list) and not isinstance(masks[0], (str, Path)):
                if _verbose_ >= 1:
                    print(f"{proc}: unexpected type '{type(masks)}' for 'masks' or")
                    print(f"{proc}: unexpected type '{type(masks[0])}' for 'masks[0]'")
                return None
        if not isinstance(kwargs["out_file_path"], (str, Path)):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(kwargs['out_file_path'])}' for 'out_file_path'!")
            return None

        # Build command parameters for subprocess call
        cmd_params = ["-images"] + images
        # Add mask images if provided
        if masks is not None:
            cmd_params = ["-masks"] + masks
        # Append output file path
        cmd_params += ["-res", kwargs["out_file_path"]]
        # Append any additional params if specified
        if params is not None and params != "":
            cmd_params += params.split(' ')
        # Execute the subprocess call and return its status code
        return subprocess.run(["meanImages"] + cmd_params).returncode

    # Call the function using the Python library if `out_file_path` is not specified
    elif isinstance(images, list):
        if _verbose_ >= 2:
            print(f"call of '{proc}': python library")

        # Validate input types for Python procedure
        if not isinstance(images[0], vtImage):
            if _verbose_ >= 1:
                print(f"{proc}: unexpected type '{type(images[0])}' for 'images[0]'")
            return None
        if masks is not None:
            if not isinstance(masks, list) and not isinstance(masks[0], vtImage):
                if _verbose_ >= 1:
                    print(f"{proc}: unexpected type '{type(masks)}' for 'masks' or")
                    print(f"{proc}: unexpected type '{type(masks[0])}' for 'masks[0]'")
                return None
            # Call the Python library method
            return vtpython._mean_masked_images(images, masks, "", params)
        # Call the Python library method
        return vtpython._mean_images(images, "", params)

    # Else, handle unexpected type for `images` parameter
    if _verbose_ >= 1:
        print(f"{proc}: unhandled type '{type(images)}' for 'images'")
    # Return `None` for unhandled types or errors
    return None


#
#
#

tools.add_doc(mean_images, vtpython._doc_mean_images())
