#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2022-2024 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Shared dataset module.
"""
import os
import tempfile
from os.path import exists
from os.path import join
from pathlib import Path
from urllib.parse import urlparse

import requests
from tqdm import tqdm

REGISTRY = {
    'flower_confocal': {
        0: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t0-imgFus.inr.gz',
            'hash': 'md5:48f6f9924289037c55ea785273c2fe72'
        },
        1: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t1-imgFus.inr.gz',
            'hash': 'md5:377dfa1b2aaf2471e5f8ab5655ef7610'
        },
        2: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t2-imgFus.inr.gz',
            'hash': 'md5:0c08a63c0966bd5655e4676893dd1068'
        }
    },
    'flower_labelled': {
        0: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t0-imgSeg.inr.gz',
            'hash': 'md5:3e9259e801c39ec5f8f235deeb8e936c'
        },
        1: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t1-imgSeg.inr.gz',
            'hash': 'md5:74f085176500e54caa7e8bf708ace12f'
        },
        2: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t2-imgSeg.inr.gz',
            'hash': 'md5:a95448cf8325836d908b55feeb71879e'
        }
    }
}


class FileLock:
    """
    Manages file-based locking mechanism to ensure mutual exclusion.

    The `FileLock` class provides a context manager to create and release a
    file-based lock. It is used to prevent concurrent access to shared
    resources in a file system by ensuring only one process can acquire
    the lock at a time. If the lock is already acquired, an exception is
    raised.

    Attributes
    ----------
    lockfile : str
        Path to the lock file used to manage the lock.
    lock_fd : int or None
        File descriptor representing the open lock file. None if the lock
        is not acquired.
    """

    def __init__(self, lockfile):
        self.lockfile = lockfile
        self.lock_fd = None

    def __enter__(self):
        """Acquires a file-based lock for ensuring exclusive access to a resource.

        The __enter__ method attempts to acquire a lock by creating a file with the specified
        lockfile name using `os.open`. If the file already exists, it raises a
        `RuntimeError` to indicate that the lock could not be acquired. This mechanism
        is commonly used for synchronizing processes and maintaining exclusive control
        over a critical resource.

        Raises
        ------
        RuntimeError
            If the lock file already exists, indicating that the lock is held by another process.
        """
        try:
            self.lock_fd = os.open(self.lockfile, os.O_CREAT | os.O_EXCL | os.O_RDWR)
        except FileExistsError as e:
            raise RuntimeError(f"Could not acquire lock: {self.lockfile}") from e

    def __exit__(self, exc_type, exc_value, traceback):
        """Handles the cleanup process when exiting a context manager.

        This method is called upon exiting a `with` statement block. It performs the
        necessary cleanup actions, which include closing a file descriptor and
        removing the lockfile if it exists. This ensures that resources are properly
        released and the lockfile is deleted after use.

        Parameters
        ----------
        exc_type : type or None
            The exception type if an exception occurred, otherwise None.
        exc_value : BaseException or None
            The exception instance if an exception occurred, otherwise None.
        traceback : TracebackType or None
            The traceback object if an exception occurred, otherwise None.
        """
        if self.lock_fd:
            os.close(self.lock_fd)
            os.unlink(self.lockfile)  # Remove lockfile


def is_valid_url(url):
    """Helper function to validate a URL.

    Parameters
    ----------
    url : str
        The URL to validate.

    Returns
    -------
    bool
        True if the URL is valid, False otherwise.
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def _fetch_or_create_temp_file(url, tmp_fname=None):
    """Check if the given URL has already been loaded.

    Parameters
    ----------
    url : str
        The URL to check.
    tmp_fname : str, optional
        Temporary file name created by `_url_image`, may be replaced by previously loaded file name if any.

    Returns
    -------
    str
        File path to temporary file to load.

    Notes
    -----
    Uses a temporary file named `timagetk_image_url.json` to store that info.

    Examples
    --------
    >>> from vt.dataset import _fetch_or_create_temp_file
    >>> url = 'https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz'
    >>> tmp_fname = _fetch_or_create_temp_file(url)
    >>> print(tmp_fname)
    /tmp/tmprwxl8e4o.inr.gz
    >>> url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> _fetch_or_create_temp_file(url)
    >>> print(tmp_fname)
    /tmp/tmpgbzdj9yd.czi

    """
    import json

    # Validate the URL
    if not is_valid_url(url):
        raise ValueError(f"Invalid URL: {url}")

    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()
    except requests.RequestException as e:
        raise ValueError(f"Failed to download URL '{url}': {e}")

    # Generate a temporary filename if not provided
    if tmp_fname is None:
        tmp_fname = get_temp_filename(url, dict(response.headers))

    tmpdir = tempfile.gettempdir()
    url_json = join(tmpdir, 'vt_image_url.json')

    lock_path = url_json + '.lock'
    url_file = {}

    # Use a file lock to ensure thread and process safety
    with FileLock(lock_path):
        # Load the JSON file if it exists
        if exists(url_json):
            try:
                with open(url_json, 'r') as json_file:
                    url_file = json.load(json_file)
            except (json.JSONDecodeError, IOError):
                # If the JSON file is invalid or unreadable, initialize to empty dict
                url_file = {}

        # If the URL is already in the JSON file and the file exists
        if url in url_file and exists(url_file[url]):
            return url_file[url]

        # Otherwise, add or update the entry in the JSON file
        url_file[url] = join(tmpdir, tmp_fname)
        try:
            with open(url_json, 'w') as json_file:
                json.dump(url_file, json_file)
        except IOError as e:
            raise ValueError(f"Failed to write to {url_json}: {e}")

    return url_file[url]


def check_hash_value(fname, hash_value):
    """Checks if the hash of the content of a file matches a given hash value and deletes the file
    if the comparison fails.

    This function takes a file name and a hash value (formatted as `hash_method:hash_value`),
    computes the hash of the file content using the specified hashing method, and compares it
    to the provided hash value. If the computed hash does not match the given hash value,
    the file is deleted, and an error is raised.

    Parameters
    ----------
    fname : str
        The name of the file whose content's hash is to be computed and validated.
    hash_value : str
        The expected hash value and method, provided in the format "hash_method:hash_value".
        For example, "sha256:abcdef123456...".

    Raises
    ------
    ValueError
        If the `hash_value` is not in the correct format, the hash method is unsupported, or the
        computed hash value does not match the provided hash value.
    FileNotFoundError
        If the specified file does not exist.
    OSError
        If an error occurs while reading the specified file.

    """
    import hashlib

    try:
        hash_method, hash_value = hash_value.split(":")
    except ValueError:
        raise ValueError("Invalid hash_value format. Expected 'hash_value:hash_method'.")

    # Instantiate hash method:
    try:
        h = hashlib.new(hash_method.lower())
    except ValueError as e:
        raise ValueError(f"Unsupported hash method '{hash_method}': {e}")

    # Compute hash based on file content:
    try:
        with open(fname, 'rb') as f:
            h.update(f.read())
    except FileNotFoundError:
        raise FileNotFoundError(f"The file '{fname}' does not exist.")
    except Exception as e:
        raise OSError(f"Error reading file '{fname}': {e}")

    # Compare hash or raise error:
    if hash_value != h.hexdigest():
        msg = f"{hash_method.upper()} comparison failed for file {fname}!\n"
        msg += f"Given hash: {hash_value}\n"
        msg += f"Computed hash: {h.hexdigest()}\n"
        Path(fname).unlink()
        raise ValueError(msg)
    return


def get_temp_filename(url, response_headers):
    """Generate a temporary filename based on URL and response headers.

    This function generates a temporary filename considering the `Content-Disposition`
    header from the response headers if available. If this header is absent, it falls
    back to using the filename derived from the URL. The generated filename is then
    appended to the path to the temporary directory.

    Parameters
    ----------
    url : str
        The URL string from which the filename will be derived if not provided
        via the response headers.
    response_headers : dict
        A dictionary of response headers that may contain a `Content-Disposition`
        key with filename information.

    Returns
    -------
    pathlib.Path
        The full path of the temporary filename.
    """
    if "Content-Disposition" in response_headers:
        content_disposition = response_headers.get("Content-Disposition")
        filename = content_disposition.split("filename=")[-1].strip('"')
    else:
        filename = Path(url).name
    return Path(tempfile.gettempdir()) / filename


def download_with_progress(response, file_path, block_size=32 * 1024):
    """Downloads a file from a response object while displaying a progress bar.

    The function writes the content from the response iteratively to the
    specified file path while updating the progress bar to reflect the
    download completion percentage. It ensures that the total downloaded size
    matches the expected total size, raising an error otherwise.

    Parameters
    ----------
    response : requests.Response
        The HTTP response object containing the content to download. It must
        support the `iter_content` method.
    file_path : pathlib.Path
        The path to the file where the downloaded content will be saved.
    block_size : int, optional
        The size of each chunk in bytes to be read and written at a time. By
        default, it is set to 32 KiB.
    """
    total_size = int(response.headers.get("content-length", 0))
    if total_size == 0:
        raise ValueError(f"Error accessing file size! Response headers: {response.headers}")

    progress = 0
    with tqdm(total=total_size, unit="B", unit_scale=True, unit_divisor=1024) as pbar:
        with open(file_path, "wb") as file:
            for chunk in response.iter_content(block_size):
                if chunk:  # Don't write empty keep-alive chunks
                    file.write(chunk)
                    progress += len(chunk)
                    pbar.update(len(chunk))
    if total_size > 0 and progress != total_size:
        raise IOError(f"Error downloading file {file_path}!")


def save_url_temp_file(url, temp_filename=None):
    """Downloads a file from the given URL and saves it as a temporary file.

    This function retrieves the content from a specified URL, ensures the content
    is a valid downloadable file (not HTML), and saves it to a temporary file.
    If no temporary filename is provided, one is automatically generated. The
    function also provides a progress indicator during the download process.

    Parameters
    ----------
    url : str
        The URL of the file to download.
    temp_filename : str, optional
        The temporary filename to save the downloaded file to.
        If not provided (default), a temporary filename is automatically generated.

    Returns
    -------
    pathlib.Path
        The path to the temporary file where the content has been saved.

    Raises
    ------
    ValueError
        If there is an issue with downloading the file or if the URL does not point
        to a valid downloadable file.
    """
    # Validate the URL
    if not is_valid_url(url):
        raise ValueError(f"Invalid URL: {url}")

    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()
    except requests.RequestException as e:
        raise ValueError(f"Failed to download URL '{url}': {e}")

    # Generate temporary filename
    if temp_filename is None:
        temp_filename = get_temp_filename(url, dict(response.headers))

    # Ensure the URL points to a valid file
    if response.headers.get("Content-Type", "").startswith("text/html"):
        raise ValueError(f"The URL '{url}' does not point to a valid file but an HTML page.")

    # Start downloading with progress
    download_with_progress(response, temp_filename)

    response.close()
    return temp_filename


def get_image_from_url(url, hash_value=None):
    """Retrieve an image from the specified URL, ensuring its integrity if a hash value is provided.

    This function downloads an image from the given URL and saves it to a temporary
    file. If the file for the URL already exists in the temporary directory, the
    download is skipped. Optionally, the integrity of the downloaded file can be
    verified using a provided hash value.

    Parameters
    ----------
    url : str
        The URL of the image to be downloaded.
    hash_value : str, optional
        The expected hash value of the image file for integrity verification. If
        provided, the function will compare the file's hash with this value.

    Returns
    -------
    pathlib.Path
        The path to local temporary file where the image is stored.

    Examples
    --------
    >>> from vt.dataset import get_image_from_url
    >>> url = 'https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz'
    >>> tmp_file = get_image_from_url(url)
    >>> print(tmp_file)
    /tmp/sphere_membrane_t0.inr.gz
    >>> # Read the temporary file image with ``vtImage``:
    >>> from vt import vtImage
    >>> img = vtImage(str(tmp_file))
    >>> print(img.shape())
    [225, 225, 225]
    >>> # Use secure hash to validate downloaded file
    >>> url = 'https://zenodo.org/records/7151866/files/p58-t0-imgFus.inr.gz'
    >>> md5_h = 'md5:48f6f9924289037c55ea785273c2fe72'
    >>> tmp_file = get_image_from_url(url, hash_value=md5_h)
    >>> print(tmp_file)
    /tmp/p58-t0-imgFus.inr.gz
    >>> # Use WRONG secure hash to INvalidate downloaded file:
    >>> wrong_md5_h = 'md5:d3079693f5d25f0743c19b004e88eaf1'
    >>> tmp_file = get_image_from_url(url, hash_value=wrong_md5_h)
    ValueError: MD5 comparison failed for file /tmp/p58-t0-imgFus.inr.gz!
    Given hash: d3079693f5d25f0743c19b004e88eaf1
    Computed hash: 48f6f9924289037c55ea785273c2fe72

    """
    # Uses the JSON file to check if we already downloaded this URL:
    tmp_fname = _fetch_or_create_temp_file(url)
    # Only save the URL to a temporary file if not already defined:
    if not exists(tmp_fname):
        save_url_temp_file(url, tmp_fname)

    # Performs hash comparison if an `hash_value` is given:
    if hash_value is not None:
        check_hash_value(tmp_fname, hash_value)

    return Path(tmp_fname)


def _append_suffix(path, suffix):
    """Appends a given suffix to the filename of the provided path object.

    Parameters
    ----------
    path : Path
        The file path.
    suffix : str
        The string to append to the filename before the file extension.

    Returns
    -------
    Path
        The updated filename.
    """
    # Extract the directory part of the path
    root = path.parent
    # Extract file extension(s), e.g., ['.tar', '.gz']
    ext = path.suffixes
    # If there are multiple extensions, join them into a single string
    if len(ext) > 1:
        ext = "".join(ext)
    else:
        # Else, use the single extension
        ext = ext[0]
    # Extract the filename without the extension(s)
    stem = path.name.replace(ext, "")
    # Construct the new filename with the suffix added before the extension(s)
    suffixed_fname = stem + suffix + ext
    # Return the new path combining the directory and the new filename
    return root / suffixed_fname


def shared_data_path(name, img_id=None):
    """Access shared dataset path from Zenodo examples.

    Parameters
    ----------
    name : str
        The dataset name to retrieve the image from.
    img_id : str or int, optional
        The identifier for the image within the dataset.

    Returns
    -------
    pathlib.Path
        The path to the Zenodo shared image from the provided dataset name and image ID.

    Raises
    ------
    ValueError
        If the dataset name is unknown.

    Examples
    --------
    >>> from vt.dataset import shared_data_path
    >>> path = shared_data_path('flower_confocal_ds2x', 0)
    >>> # Example - Get a 3D confocal image of an early flower of Arabidopsis thaliana
    >>> path = shared_data_path('flower_confocal', 0)
    >>> print(path)
    $HOME/.cache/vtpython/44b7f3d0bccef67a517cd55ad4e6a9ff-p58-t0-imgFus.inr.gz
    >>> path = shared_data_path('flower_confocal')
    >>> print(path)
    {0: '$HOME/.cache/vtpython/44b7f3d0bccef67a517cd55ad4e6a9ff-p58-t0-imgFus.inr.gz', 1: '$HOME/.cache/vtpython/d47e26514b326cb3464fe7fbcd95b7f4-p58-t1-imgFus.inr.gz', 2: '$HOME/.cache/vtpython/c381fb2c79e6b6cad13e14f480e6ea08-p58-t2-imgFus.inr.gz'}

    """
    import numpy as np
    from vt import apply_trsf
    from vt import create_trsf
    from vt import vtImage

    # Check if the name contains the '_ds2x' suffix, indicating a downsampled version
    if '_ds2x' in name:
        # Remove the '_ds2x' suffix from the name
        name = name.replace('_ds2x', '')
        # Recursively call `shared_data_path` to get the original image path
        path = shared_data_path(name, img_id=img_id)
        # Append '_ds2x' to the original path to create the new downsampled path
        ds_path = _append_suffix(path, '_ds2x')
        # Load the original image
        in_im = vtImage(str(path))
        # Calculate the new voxel size by doubling the original voxel size
        new_vxs = np.array(in_im.spacing()) * 2.0
        # Apply transformation with the new voxel size and save the downsampled image
        id_trsf = create_trsf(params=' -transformation-value identity -trsf-type affine')
        ds_im = apply_trsf(in_im, id_trsf,
                           params=f" -voxel-size {' '.join(map(str, new_vxs))} -resize -interpolation linear")
        # Write the downsampled image to disk
        ds_im.write(str(ds_path))
        # Return the path of the downsampled image
        return ds_path

    # Check if the dataset name is known in the registry
    if name in REGISTRY:
        # If no image ID is provided, return all images in the dataset
        if img_id is None:
            dataset = REGISTRY[name]
            return {idx: get_image_from_url(data['url'], hash_value=data['hash']) for idx, data in dataset.items()}
        # If an image ID is provided, return the specific image from the dataset
        elif img_id in REGISTRY[name]:
            return get_image_from_url(REGISTRY[name][img_id]['url'], hash_value=REGISTRY[name][img_id]['hash'])
        # Raise an error if the provided image ID is not found in the dataset
        else:
            raise ValueError(f"Unknown ID '{img_id}' for dataset name '{name}'!")
    # Raise an error if the provided dataset name is not found in the registry
    else:
        raise ValueError(f"Unknown dataset name '{name}'!")


def shared_data(name, img_id):
    """Access shared dataset, either synthetic data or Zenodo examples.

    Parameters
    ----------
    name : str
        The dataset name to retrieve the image from.
    img_id : str or int
        The identifier for the image within the dataset.

    Returns
    -------
    SpatialImage or LabelledImage
        The image read from the provided dataset name and image ID.

    Raises
    ------
    ValueError
        If the dataset name is unknown.

    Examples
    --------
    >>> from vt.dataset import shared_data
    >>> # Example 1 - Get a 3D confocal image of an early flower of Arabidopsis thaliana
    >>> img = shared_data('flower_confocal', 0)
    >>> print(img.shape())
    [460, 460, 320]
    >>> # Example 2 - Get a 3D labelled image of an early flower of Arabidopsis thaliana
    >>> seg_img = shared_data('flower_labelled', 0)
    >>> print(seg_img.shape())
    [460, 460, 320]

    """
    from vt import vtImage
    return vtImage(str(shared_data_path(name, img_id)))


def list_shared_data():
    """Lists the shared dataset names.

    Returns
    -------
    dict
        A dictionary of dataset names and time-points available in the shared image registry.

    Examples
    --------
    >>> from vt.dataset import list_shared_data
    >>> list_shared_data()
    {'flower_confocal': [0, 1, 2], 'flower_labelled': [0, 1, 2]}

    """
    return {k: sorted(list(v.keys())) for k, v in REGISTRY.items()}
