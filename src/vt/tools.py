#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np


def add_doc(py_func, s):
    py_doc = py_func.__doc__
    new_doc = "{0}\n\n**C command line help**\n\n{1}\n".format(py_doc, s)
    py_func.__doc__ = new_doc


def random_array(shape, dtype="uint8"):
    """Generate a random array.

    Parameters
    ----------
    shape : list(int)
        Shape of the array to create.
    dtype : str in {"uint8", "uint16", "float32", "float", "double"}
        Numerical types of the values.

    Returns
    -------
    numpy.ndarray
        The random array.

    Notes
    -----
    Numpy type `float32` correspond to `float` in C.
    Numpy type `float64` correspond to `double` in C.
    Numpy type `float` correspond to `float64`.

    References
    ----------
    .. _NumPy array types and conversions between types:
        https://docs.scipy.org/doc/numpy/user/basics.types.html#array-types-and-conversions-between-types

    Examples
    --------
    >>> from vt.tools import random_array
    >>> rand_image = random_array([7, 5], dtype='uint8')
    >>> print(rand_image.shape)
    >>> print(rand_image.dtype)

    >>> rand_image = random_array([7, 5], dtype='float')
    >>> print(rand_image.shape)
    >>> print(rand_image.dtype)

    >>> rand_image = random_array([7, 5, 3], dtype='uint8')
    >>> print(rand_image.shape)
    >>> print(rand_image.dtype)

    >>> rand_image = random_array([7, 5, 3], dtype='double')
    >>> print(rand_image.shape)
    >>> print(rand_image.dtype)

    """
    # Convert given str to NumPy type:
    dtype = np.dtype(dtype)

    # Create the array given the sub-type: integer or float
    if np.issubdtype(dtype, np.integer):
        mini = np.iinfo(dtype).min
        maxi = np.iinfo(dtype).max
        arr = np.random.randint(low=mini, high=maxi, size=shape, dtype=dtype)
    else:
        arr = np.random.random_sample(shape).astype(dtype)

    return arr
