# Change Log

## version 1.3.0

This version requires `vt` package version `1.6.0` or higher.

### Code Updates

- Introduced `dataset.py` to manage and retrieve datasets from remote URLs the `pooch` package for cached data.
- Renamed `vtCellProperty` class to `CellProperty` to match Python class naming guidelines.
  - Implemented `ppty_list` parameter and default property list for consistency with C++ implementation.
  - Added `labels`, `neighbor`, and `neighbor_number` methods.
- Refactor Various Bridge Classes for Code Clarity:
  - `vtImageBridgePython`
    - Improved code readability with comments and documentation.
    - Renamed `getNeighborsLabels` to `getNeighborLabels` for naming consistency with C++ implementation.
    - Added `getNeighborNumber` binding.
  - `BridgePython`: Improved code readability with added comments and documentation.
  - `vtTransformationBridgePython`: Improved code readability with added comments and documentation.
- Improved docstrings for clarity and consistency across various functions.
- Enhanced parameter and return type validations with added examples.
- Removed duplicate imports and added explanations for code blocks.

### Code Improvements

- Removed unnecessary `print` statements in the `linear_filter` function.
- Corrected docstring examples in the `apply_trsf` function.
- Made minor fixes to the conda-build recipe.
- Fixed the conda DOC environment recipe by updating the lower Python version to 3.8.
- Updated the conda DEV environment recipe by adjusting the Python version requirement and removing documentation related packages, which were shifted to vt-python-doc.yaml.
- Updated the README file for shorter and clearer conda instructions.

### Test Updates

- Standardized the testing framework across all tested modules using `unittest`.
- Added specific tests for various core functionalities including transformations, filters, and algorithms.
- Added test for the new `Image` class.

### Continuous Integration (CI) Updates

- Updated CI job recipes to ensure consistency, functionality & debugging.
- Implemented a `clean-conda-*` CI job for each OS to clean packages, built packages, and remove project environment.
- Updated the pages CI job to include make as a required package for building the Sphinx documentation.

## version 1.2.0

- documentation framework
- move CI from jenkins to gitlab-runner
- automatic build of conda package and upload to anaconda.org
- update build system configuration (both CMake and setuptools)
- use C++20 compile flags

## version 1.0.0

- add python interfaces to VT C++ interfaces through pybind11 and
xtensor
- write python functions
- add tests and examples

## version 0.0.0
