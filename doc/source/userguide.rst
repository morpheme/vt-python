------------
User's guide
------------

Images
======

Image creation
--------------

``vt-python`` class ``vtImage`` is designed to manipulate images.

An image can be created either by reading an image file

.. code-block:: python

   >>> import vt
   >>> im = vt.vtImage("myimagefile.mha")

or from a ``numpy`` array

.. code-block:: python

   >>> import vt
   >>> import numpy as np
   >>> buf_ones = np.ones((3,4,5))
   >>> buf_ones.shape
   (3, 4, 5)
   >>> im_ones = vt.vtImage(buf_ones)
   >>> im_ones.shape()
   [5, 4, 3]

.. note::
   Notice that the axis order of ``numpy`` array returned by ``shape``
   is Z, Y, X (for 3D images), while it is X, Y, Z for the ``shape()``
   method of ``vtImage``.

.. warning::
   When creating an image from a ``numpy`` array, the memory is shared
   between the ``numpy`` array and the ``vtImage``. It implies that,
   if the ``numpy`` array is modified *after* the ``vtImage`` creation,
   the ``vtImage`` is however modified.

Set the spacing (voxel size) can be done with the ``setSpacing()`` method.

   >>> import vt
   >>> import numpy as np
   >>> buf_ones = np.ones((3,4,5))
   >>> im_ones = vt.vtImage(buf_ones)
   >>> im_ones.spacing()
   [1.0, 1.0, 1.0]
   >>> im_ones.setSpacing([1,2,3])
   >>> im_ones.spacing()
   [1.0, 2.0, 3.0]

.. note::
   The spacing order is the same than for the ``shape()``
   method of ``vtImage``, i.e. X, Y, Z

Image I/O
---------

Reading an image file is done by creating a ``vtImage``.

.. code-block:: python

   >>> import vt
   >>> im = vt.vtImage("myimagefile.mha")

Writing an image file is done by using the ``write()`` method.

.. code-block:: python

   >>> import vt
   >>> im = vt.vtImage("myimagefile.mha")
   >>> im.write("foo.nii")

When writing, image format is set by the filename name extension.
A few image formats are handled

* 2D images

  - PNM / PGM

* 3D images

  - Analyze ('.hdr' extension)
  - Inrimage ('.inr' extension): it is an home-made format from INRIA, thus non standard
  - Nifti ('.nii' extension)
  - Tiff ('.tif' extension): although it can handled 3D images

In addition gziped images (with additionnal '.gz' extension) can also
be read and written.

Image <-> numpy array
---------------------

An image can be created from a ``numpy`` array

.. code-block:: python

   >>> import vt
   >>> import numpy as np
   >>> buf_ones = np.ones((3,4,5))
   >>> buf_ones.shape
   (3, 4, 5)
   >>> im_ones = vt.vtImage(buf_ones)
   >>> im_ones.shape()
   [5, 4, 3]

.. note::
   Notice that the axis order of ``numpy`` array returned by ``shape``
   is Z, Y, X (for 3D images), while it is X, Y, Z for the ``shape()``
   method of ``vtImage``.

.. warning::
   When creating an image from a ``numpy`` array, the memory is shared
   between the ``numpy`` array and the ``vtImage``. It implies that,
   if the ``numpy`` array is modified *after* the ``vtImage`` creation,
   the ``vtImage`` is however modified.

Conversely, a ``numpy`` array can be obtained from a ``vtImage``.
