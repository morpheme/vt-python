=====================================
Welcome to vt-python's documentation!
=====================================

.. image:: https://anaconda.org/morpheme/vt-python/badges/version.svg
   :target: https://anaconda.org/morpheme/vt-python

.. image:: https://anaconda.org/morpheme/vt-python/badges/latest_release_date.svg
   :target: https://anaconda.org/morpheme/vt-python

.. image:: https://anaconda.org/morpheme/vt-python/badges/platforms.svg
   :target: https://anaconda.org/morpheme/vt-python

.. image:: https://anaconda.org/morpheme/vt-python/badges/license.svg
   :target: https://anaconda.org/morpheme/vt-python

.. image:: https://anaconda.org/morpheme/vt-python/badges/downloads.svg
   :target: https://conda.anaconda.org/morpheme/vt-python

``vt-python`` contains python wrappers for the C library `vt <https://gitlab.inria.fr/morpheme/vt>`_.

.. toctree::
   :maxdepth: 3
   :caption: Contents
   :numbered:

   releasenotes.rst
   installation.rst
   userguide.rst
   reference.rst
