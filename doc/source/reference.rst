Reference API
=============

.. toctree::
   :maxdepth: 2

   ref_apply_trsf.rst
   ref_apply_trsf_to_points.rst
   ref_blockmatching.rst
   ref_cellfilter.rst
   ref_compose_trsf.rst
   ref_connexe.rst
   ref_create_trsf.rst
   ref_inv_trsf.rst
   ref_linear_filter.rst
   ref_mean_images.rst
   ref_mean_trsfs.rst
   ref_morpho.rst
   ref_pointmatching.rst
   ref_regionalext.rst
   ref_watershed.rst
