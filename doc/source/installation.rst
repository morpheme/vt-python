------------
Installation
------------
   
Was tested only on Linux or MacOs systems.


User installation 
=================

Using conda, you can easily install the ``vt-python`` library and its dependencies, 
in a newly created environment named ``vt-python``, as follow:

    .. code-block:: bash

       conda install vt-python -n vt-python -c morpheme -c conda-forge

.. note::
   * ``-n vt-python`` indicates the environment name you are creating. You may choose any other name.
   * ``-c morpheme`` indicates the morpheme channel in `Anaconda Cloud <https://anaconda.org/>`_.

``ipython`` may ease your use of python. It can be installed with

    .. code-block:: bash

       conda activate vt-python
       conda install ipython -n vt-python

``vt-python`` is now ready to be used from ``python`` in the ``vt-python`` conda environment

    .. code-block:: python

       import vt
       img = vt.vtImage("<my_image.tif>")


Developer installation 
======================

First you have to decide if you also need to install the sources for the C library ``vt``.

``vt`` installation
-------------------

``vt`` source installation
~~~~~~~~~~~~~~~~~~~~~~~~~~

If so, follow ``vt`` `sources install instructions <https://gitlab.inria.fr/morpheme/vt>`_.

Then update ``vt-python`` environment (created during the `vt` source install) with `vt-python` dependencies:

    .. code-block:: bash
       
       conda env update -n vt-python --file vt-python/pkg/env/vt-python-dev.yaml

.. note::
  ``vt-python/pkg/env/vt-python_dev.yaml`` contains all required dependencies and channels.



``vt`` conda package installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can also use the conda environment recipe for ``vt-python``:

    .. code-block:: bash
       
       conda env create -n vt-python -f vt-python/pkg/env/vt-python.yaml

.. note::
   this will create an environment named `vt-python`.


Clone `vt-python` sources
-------------------------

Then clone the sources:

    .. code-block:: bash

       git clone https://gitlab.inria.fr/morpheme/vt-python.git


Install `vt-python` sources
---------------------------

To install `vt-python` sources in the ``vt-python`` environment, simply run: 

    .. code-block:: bash
 
       conda activate vt-python
       cd vt-python
       python setup.py develop

.. note::
   Do not forget to clean the `build` directory -if any- before installing.

   .. code-block:: bash

      cd vt-python
      if [[ -d build ]]
      then
        rm -rf build
      fi


Contributing
============

Build & upload conda package
----------------------------

.. warning::
   this should be done from the `base` environment!

   .. code-block:: bash
  
      conda activate base


Install ``conda-build``, and ``anaconda-client`` if you want to upload your package:

.. code-block:: bash

  conda install conda-build anaconda-client

.. warning::
   For macOS, follow these 
   `instructions <https://docs.conda.io/projects/conda-build/en/latest/resources/compiler-tools.html#macos-sdk>`_ 
   to install the required `macOS 10.9 SDK`.


Using the given recipe, it is easy to build the `vt-python` conda package:

.. code-block:: bash

  cd vt-python
  conda build -c conda-forge -c morpheme .

.. note::
   ``-c conda-forge`` and ``-c morpheme`` are required for proper channels definition.


Optionally, to upload you package on `Anaconda Cloud`_ you will need a valid account.
Follow this `procedure <https://docs.conda.io/projects/conda-build/en/latest/user-guide/tutorials/build-pkgs-skeleton.html#optional-uploading-packages-to-anaconda-org>`_.

Then, you can build and automatically upload to your account or organization upon completion:

.. code-block:: bash

   cd vt-python
   conda build -c conda-forge -c morpheme . --user morpheme

.. note::
   you may want to change the ``user``.


Running tests
-------------

If you want to **run the tests**, you will have to install ``nose`` and ``coverage`` to get report on tests coverage.

* `nose <http://nose.readthedocs.io/en/latest/>`_: testing, all platforms
* `coverage <http://coverage.readthedocs.org/>`_: testing coverage, all platforms

To install them, you can use the conda packages:

.. code-block:: bash

   conda install nose coverage -n morpheme


Then, from the root folder, run:

.. code-block:: bash

   cd tst
   nosetests -v




