import filecmp
import tempfile
import unittest
from pathlib import Path

import vt
from vt.dataset import shared_data_path


class TestMorpho(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path("flower_confocal", 0)
        cls._in_image = vt.vtImage(str(cls._in_path))

    def _morpho(self, params):
        # Test method with `vtImage` as input (bridge):
        out_image = vt.morpho(self._in_image, params)
        # Make sure the returned `vtImage` is not `None`:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_image.write(py_file.name)

        # Test method with 'path to image' as input (cli):
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            cmd_status = vt.morpho(self._in_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_morpho_dilation_sphere(self):
        self._morpho("-dilation -sphere -radius 3 -iterations 1")

    def test_morpho_erosion(self):
        self._morpho("-erosion -iterations 2")
