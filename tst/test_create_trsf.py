import filecmp
import tempfile
import unittest
from pathlib import Path

import vt


class TestCreateTrsf(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    def _create(self, params=None):
        #
        # Run "structure" `create_trsf` with given params:
        # since the first argument of vt.create_trsf is fixedpoint_image
        # we have to explicitly set params to the variable 'params'
        #
        out_trsf = vt.create_trsf(params=params)
        # Make sure the returned trsf is not None:
        self.assertIsNotNone(out_trsf)
        # Write the "structure" transformation file:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_trsf.write(py_file.name)
        del out_trsf

        # Run "command-line" `create_trsf` with given params:
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            cmd_status = vt.create_trsf(params=params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "command-line" & "structure" transformation files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_create_trsf_linear_identity(self):
        self._create('-identity')

    def test_create_trsf_linear_random(self):
        self._create('-random -srandom ' + str(1))

    def test_create_trsf_non_linear2d_random(self):
        self._create('-type vectorfield2D -value sinus2D -dim 20 20 -vs 0.25 0.25')

    def test_create_trsf_non_linear3d_random(self):
        self._create('-type vectorfield3D -value sinus3D -dim 20 20 10 -vs 0.25 0.25 0.5')
