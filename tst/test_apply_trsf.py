import contextlib
import filecmp
import os
import unittest
from pathlib import Path

import numpy as np

import vt
from vt.dataset import shared_data_path


@contextlib.contextmanager
def temporary_filename(suffix=None):
    """Context that introduces a temporary file.

    from : https://stackoverflow.com/questions/3924117/how-to-use-tempfile-namedtemporaryfile-in-python
    Creates a temporary file, yields its name, and upon context exit, deletes it.
    (In contrast, tempfile.NamedTemporaryFile(delete=False) provides a 'file' object and
    deletes the file as soon as that file object is closed, so the temporary file
    cannot be safely re-opened by another library or process.)

    Args:
      suffix: desired filename extension (e.g. '.mp4').

    Yields:
      The name of the temporary file.
    """
    import tempfile
    try:
        f = tempfile.NamedTemporaryFile(suffix=suffix, delete=False)
        # os.path.join(tempfile.gettempdir(), os.urandom(24).hex())
        tmp_name = f.name
        f.close()
        yield f
    finally:
        os.unlink(tmp_name)


class TestApplyTrsf(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path('flower_confocal_ds2x', 0)
        cls._in_image = vt.vtImage(str(cls._in_path))

        trsf = vt.create_trsf(fixedpoint_image=cls._in_image, params="-random -trsf-type rigid")
        #
        # l'image d'entree est ../data/p58-t0_INT_down_interp_2x.inr
        # on garde le nom (pour la commande en ligne)
        # on lit l'image (pour le wrapping python)
        # on cree une transformation aleatoire que l'on ecrit et relit
        # (pour etre sur que ce soit la "meme" transformation utilisee
        #  dans les deux runs a comparer - sinon, il y a des arrondis)
        #
        with temporary_filename() as trsf_file:
            cls._trsf_file = trsf_file
            trsf.write(str(cls._trsf_file.name))
            cls._trsf = vt.vtTransformation(cls._trsf_file.name)

    @classmethod
    def tearDownClass(cls):
        trsf_path = Path(cls._trsf_file.name)
        trsf_path.unlink(missing_ok=True)

    def _apply(self, ref_path, ref_image, params=""):
        #
        # Apply the randomly generated transformation to the image:
        # Run "structure" apply_trsf:
        #

        out_image = vt.apply_trsf(self._in_image, self._trsf, ref=ref_image, params=params)
        # Make sure the returned image is not None:
        self.assertIsNotNone(out_image)

        #
        # temporary_filename cree un fichier temporaire qui sera efface
        # apres fermeture
        #
        with temporary_filename() as cmd_file, temporary_filename() as trsf_file:
            self._trsf.write(trsf_file.name)

            #
            # mettre un "vrai" nom de fichier dans 'out_file_path'
            # (a la place de cmd_file.name) pour garder le resultat
            # eg: out_file_path="/Users/greg/TMP/cmd_apply_trsf.mha"
            #
            # Run "command-line" apply_trsf:
            cmd_status = vt.apply_trsf(self._in_path, trsf_file.name, ref=ref_path,
                                       out_file_path=cmd_file.name, params=params)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

            with temporary_filename() as py_file:
                #
                # mettre un "vrai" nom de fichier a la place 'py_file.name' pour garder le resultat
                # eg: out_image.write("/Users/greg/TMP/py_apply_trsf.mha")
                #
                # Write the "structure" image file:
                out_image.write(py_file.name)
                # Compare "command-line" & "structure" image files:
                self.assertTrue(
                    filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(py_file.name).unlink(missing_ok=True)
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(trsf_file.name).unlink(missing_ok=True)
        return out_image

    #
    # all methods beginning by 'test_' will be launched by 'conda build'
    #
    def test_apply_trsf_without_ref(self):
        out_image = self._apply(ref_path=None, ref_image=None)
        self.assertListEqual(self._in_image.shape(), out_image.shape())
        self.assertListEqual(self._in_image.spacing(), out_image.spacing())
        self.assertTrue(np.max(out_image.copy_to_array()) > 0)

    def test_apply_trsf_with_ref(self):
        ref_path = shared_data_path("flower_confocal_ds2x", 1)
        ref_image = vt.vtImage(str(ref_path))

        out_image = self._apply(ref_path=ref_path, ref_image=ref_image)

        self.assertListEqual(ref_image.shape(), out_image.shape())
        self.assertListEqual(ref_image.spacing(), out_image.spacing())
        self.assertTrue(np.max(out_image.copy_to_array()) > 0)

    def test_apply_trsf_with_ref_linear(self):
        ref_path = shared_data_path("flower_confocal_ds2x", 1)
        ref_image = vt.vtImage(str(ref_path))
        out_image = self._apply(ref_path=ref_path, ref_image=ref_image, params="-linear")
        self.assertListEqual(ref_image.shape(), out_image.shape())
        self.assertListEqual(ref_image.spacing(), out_image.spacing())
        self.assertTrue(np.max(out_image.copy_to_array()) > 0)

    def test_apply_trsf_with_ref_cspline(self):
        ref_path = shared_data_path("flower_confocal_ds2x", 1)
        ref_image = vt.vtImage(str(ref_path))
        out_image = self._apply(ref_path=ref_path, ref_image=ref_image, params="-cspline")
        self.assertListEqual(ref_image.shape(), out_image.shape())
        self.assertListEqual(ref_image.spacing(), out_image.spacing())
        self.assertTrue(np.max(out_image.copy_to_array()) > 0)
