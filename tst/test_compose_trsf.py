import filecmp
import tempfile
import unittest
from pathlib import Path

import vt


class TestComposeTrsf(unittest.TestCase):

    def _create(self, params):
        #
        # on cree un tableau de deux transformations,
        # l'une aleatoire et l'autre sont inverse
        # que l'on ecrit et relit pour eviter les problemes d'arrondis
        #
        trsfs = []

        #
        # since the first argument of vt.create_trsf is fixedpoint_image
        # we have to explicity set params to the variable 'params'
        #
        trsf = vt.create_trsf(params=params)
        with tempfile.NamedTemporaryFile(delete=False) as trsf_file:
            trsf.write(trsf_file.name)
            trsfs.append(vt.vtTransformation(trsf_file.name))

        trsf = vt.inv_trsf(trsf, params="-noverbose")
        with tempfile.NamedTemporaryFile(delete=False) as trsf_file:
            trsf.write(trsf_file.name)
            trsfs.append(vt.vtTransformation(trsf_file.name))

        # to write transformations on disk
        # trsfs[0].write("/Users/greg/TMP/trsf1.trsf")
        # trsfs[1].write("/Users/greg/TMP/trsf2.trsf")

        return trsfs

    def test_compose_trsf_linear(self):
        trsfs = self._create("-random")

        # Run "structure" `create_trsf` with given params:
        out_trsf = vt.compose_trsf(trsfs)
        # Make sure the returned trsf is not None:
        self.assertIsNotNone(out_trsf)
        # TODO: test equality to identity?

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as left_file, \
                tempfile.NamedTemporaryFile(delete=False) as right_file:
            trsfs[0].write(left_file.name)
            trsfs[1].write(right_file.name)
            # Run "command-line" `create_trsf` with same params:
            cmd_status = vt.compose_trsf([left_file.name, right_file.name], out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                # Write the "structure" transformation file:
                out_trsf.write(py_file.name)
                # Compare "command-line" & "structure" transformation files:
                self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(left_file.name).unlink(missing_ok=True)
        Path(right_file.name).unlink(missing_ok=True)

    def test_compose_trsf_non_linear2d(self):
        trsfs = self._create("-type vectorfield2D -value sinus2D -dim 20 20 -vs 0.25 0.25")

        # Run "structure" `create_trsf` with given params:
        out_trsf = vt.compose_trsf(trsfs)
        # Make sure the returned trsf is not None:
        self.assertIsNotNone(out_trsf)

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as left_file, \
                tempfile.NamedTemporaryFile(delete=False) as right_file:
            trsfs[0].write(left_file.name)
            trsfs[1].write(right_file.name)
            # Run "command-line" `create_trsf` with same params:
            cmd_status = vt.compose_trsf([left_file.name, right_file.name], out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                # Write the "structure" transformation file:
                out_trsf.write(py_file.name)
                # Compare "command-line" & "structure" transformation files:
                self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(left_file.name).unlink(missing_ok=True)
        Path(right_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_compose_trsf_non_linear3d(self):
        trsfs = self._create("-type vectorfield3D -value sinus3D -dim 20 20 10 -vs 0.25 0.25 0.5")

        # Run "structure" `create_trsf` with given params:
        out_trsf = vt.compose_trsf(trsfs)
        # Make sure the returned trsf is not None:
        self.assertIsNotNone(out_trsf)

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as left_file, \
                tempfile.NamedTemporaryFile(delete=False) as right_file:
            trsfs[0].write(left_file.name)
            trsfs[1].write(right_file.name)
            # Run "command-line" `create_trsf` with same params:
            cmd_status = vt.compose_trsf([left_file.name, right_file.name], out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                # Write the "structure" transformation file:
                out_trsf.write(py_file.name)
                # Compare "command-line" & "structure" transformation files:
                self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(cmd_file.name).unlink(missing_ok=True)
        Path(left_file.name).unlink(missing_ok=True)
        Path(right_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)
