import filecmp
import tempfile
import unittest
from pathlib import Path

from vt import blockmatching
from vt import vtImage
from vt.dataset import shared_data_path


class TestBlockmatching(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._ref_path = shared_data_path("flower_confocal_ds2x", 1)
        cls._flo_path = shared_data_path('flower_confocal_ds2x', 0)
        cls._ref_image = vtImage(str(cls._ref_path))
        cls._flo_image = vtImage(str(cls._flo_path))

    def _blockmatching(self, params):
        # Test method with `vtImage` as input (bridge):
        out_trsf = blockmatching(self._flo_image, self._ref_image,
                                 params=params)
        # Make sure the returned `vtTransformation` is not `None`:
        self.assertIsNotNone(out_trsf)
        # Save the output transformation:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_trsf.write(py_file.name)
        del out_trsf

        # Test method with 'path to images' as input (cli):
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            # Run "command-line" `blockmatching` with same params:
            cmd_status = blockmatching(self._flo_path, self._ref_path,
                                       params=params,
                                       out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    #
    # all methods beginning by 'test_' will be launched by 'conda build'
    #
    def test_blockmatching_rigid(self):
        self._blockmatching('-trsf-type rigid -py-ll 3 -no-verbose')

    def test_blockmatching_affine(self):
        self._blockmatching('-trsf-type affine -py-ll 3 -no-verbose')

    def test_blockmatching_vectorfield(self):
        self._blockmatching('-trsf-type vectorfield -py-ll 3 -no-verbose')
