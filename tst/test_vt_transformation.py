import filecmp
import tempfile
import unittest
from pathlib import Path

import numpy as np

from vt import create_trsf
from vt import vtTransformation


class TestVtTransformation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Initialize random number generator
        rng = np.random.default_rng(12345)
        # Generate a 4x4 array of random double precision floating point numbers
        cls.rand_double_points = rng.random((4, 4), dtype="double")
        # Generate a 4x4 array of random floating point numbers
        cls.rand_float_points = rng.random((4, 4), dtype="float")

    def test_vt_transformation_vt2np2vt(self):
        #
        # since the first argument of vt.create_trsf is fixedpoint_image
        # we have to explicity set params to the variable 'params'
        #
        vt_trsf = create_trsf(params="-random")
        a = vt_trsf.copy_to_array()
        t = vtTransformation(a)
        with tempfile.NamedTemporaryFile(delete=False) as file1, tempfile.NamedTemporaryFile(delete=False) as file2:
            vt_trsf.write(file1.name)
            t.write(file2.name)
            self.assertTrue(filecmp.cmp(file1.name, file2.name, shallow=False))
        Path(file1.name).unlink(missing_ok=True)
        Path(file2.name).unlink(missing_ok=True)

    def _np_vt_np(self, a):
        trsf = vtTransformation(a)
        b = trsf.copy_to_array()
        self.assertTrue((a == b).all())

    def test_vt_transformation_nparray_double(self):
        self._np_vt_np(self.rand_double_points)

    def test_vt_transformation_nparray_float(self):
        self._np_vt_np(self.rand_float_points)


class TestVtTransformationDummy(unittest.TestCase):

    def test_setUnit(self):
        trsf = vtTransformation()
        trsf.setUnit("voxel")
        self.assertEqual(trsf.unit(), "voxel")

    def test_invalid_array_dimension(self):
        array = np.array([1.0, 0.0, 0.0])  # Invalid dimension (1D)
        with self.assertRaises(ValueError):
            vtTransformation(array)

    def test_invalid_array_shape(self):
        array = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])  # Invalid shape (3x3)
        with self.assertRaises(ValueError):
            vtTransformation(array)

    def test_print(self):
        trsf = vtTransformation(np.eye(4))
        trsf.print()
