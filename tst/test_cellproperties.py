import tempfile
import unittest
from pathlib import Path

import numpy as np

from vt.cellproperties import CellProperties
from vt.dataset import shared_data


class TestCellPropertiesFromPath(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_image = shared_data("flower_labelled", 0)
        cls.ppty = CellProperties(cls._in_image, filter_neighbors=True)

    def test_n_labels(self):
        results = self.ppty.n_labels()
        self.assertTrue(results == 1040)
        self.assertTrue(isinstance(results, int))

    def test_labels(self):
        results = self.ppty.labels()
        self.assertTrue(len(results) == 1040)
        self.assertTrue(isinstance(results, list))

    def test_all_neighbor(self):
        results = self.ppty.neighbor()
        self.assertTrue(len(results) == 1040)
        for label, neighbor in results.items():
            self.assertIsInstance(neighbor, list)

    def test_labels_neighbor(self):
        results = self.ppty.neighbor([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], list) for l in [4, 5]))

    def test_label_neighbor(self):
        self.assertListEqual(list(self.ppty.neighbor(4)),
                             [1, 445, 241, 158, 998, 337, 390, 142, 5, 901, 31, 153])

    def test_all_neighbor_number(self):
        result = self.ppty.neighbor_number()
        self.assertTrue(len(result) == 1040)
        for label, n_neighbor in result.items():
            self.assertIsInstance(n_neighbor, int)

    def test_labels_neighbor_number(self):
        results = self.ppty.neighbor_number([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], int) for l in [4, 5]))

    def test_label_neighbor_number(self):
        self.assertTrue(self.ppty.neighbor_number(4) == 12)

    def test_all_volume(self):
        result = self.ppty.volume()
        self.assertTrue(len(result) == 1040)
        for label, volume in result.items():
            self.assertIsInstance(volume, int)

    def test_labels_volume(self):
        results = self.ppty.volume([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], int) for l in [4, 5]))

    def test_label_volume(self):
        self.assertTrue(self.ppty.volume(4) == 24373)

    def test_all_surface(self):
        results = self.ppty.surface()
        self.assertTrue(len(results) == 1040)
        for label, surface in results.items():
            self.assertIsInstance(surface, float)

    def test_labels_surface(self):
        results = self.ppty.surface([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], float) for l in [4, 5]))

    def test_label_surface(self):
        self.assertTrue(self.ppty.surface(4) == 4767.4716796875)

    def test_all_barycenter(self):
        results = self.ppty.barycenter()
        self.assertTrue(len(results) == 1040)
        for label, barycenter in results.items():
            self.assertTrue(isinstance(barycenter, np.ndarray) and barycenter.shape == (3,))

    def test_labels_barycenter(self):
        results = self.ppty.barycenter([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], np.ndarray) and results[l].shape == (3,) for l in [4, 5]))

    def test_label_barycenter(self):
        result = self.ppty.barycenter(4)
        self.assertTrue(isinstance(result, np.ndarray))
        self.assertTrue(result.shape == (3,))

    def test_all_covariance(self):
        results = self.ppty.covariance()
        self.assertTrue(len(results) == 1040)
        for label, res in results.items():
            self.assertTrue(isinstance(res, np.ndarray) and res.shape == (3,3))

    def test_labels_covariance(self):
        results = self.ppty.covariance([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], np.ndarray) and results[l].shape == (3,3) for l in [4, 5]))

    def test_label_covariance(self):
        result = self.ppty.covariance(4)
        self.assertTrue(isinstance(result, np.ndarray))
        self.assertTrue(result.shape == (3,3))

    def test_all_eigenvalue(self):
        results = self.ppty.eigenvalue()
        self.assertTrue(len(results) == 1040)
        for label, eigenvalue in results.items():
            self.assertTrue(isinstance(eigenvalue, np.ndarray) and eigenvalue.shape == (3,))

    def test_labels_eigenvalue(self):
        results = self.ppty.eigenvalue([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], np.ndarray) and results[l].shape == (3,) for l in [4, 5]))

    def test_label_eigenvalue(self):
        result = self.ppty.eigenvalue(4)
        self.assertTrue(isinstance(result, np.ndarray))
        self.assertTrue(result.shape == (3,))

    def test_all_eigenvector(self):
        results = self.ppty.eigenvector()
        self.assertTrue(len(results) == 1040)
        for label, eigenvector in results.items():
            self.assertTrue(isinstance(eigenvector, np.ndarray) and eigenvector.shape == (3,3))

    def test_labels_eigenvector(self):
        results = self.ppty.eigenvector([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], np.ndarray) and results[l].shape == (3,3) for l in [4, 5]))

    def test_label_eigenvector(self):
        result = self.ppty.eigenvector(4)
        self.assertTrue(isinstance(result, np.ndarray))
        self.assertTrue(result.shape == (3,3))

    def test_all_contactsurface(self):
        results = self.ppty.contactsurface()
        self.assertTrue(len(results) == 1040)
        for label, contactsurfaces in results.items():
            self.assertTrue(isinstance(contactsurfaces, dict))
            self.assertTrue(all(isinstance(v, float) for v in contactsurfaces.values()))

    def test_labels_contactsurface(self):
        results = self.ppty.contactsurface([4, 5])
        self.assertTrue(isinstance(results, dict))
        self.assertTrue(len(results) == 2)
        self.assertTrue(all(isinstance(results[l], dict) for l in [4, 5]))
        self.assertTrue(all(isinstance(v, float) for r in results.values() for v in r.values()))

    def test_label_contactsurface(self):
        self.assertTrue(self.ppty.contactsurface(4)[1] == 1245.115234375)

    def test_write(self):
        # Create a temporary file without keeping it open
        with tempfile.NamedTemporaryFile(delete=False) as file:
            temp_file_name = file.name

        try:
            self.ppty.write(temp_file_name)
            self.assertTrue(Path(temp_file_name).is_file())
            self.assertTrue(Path(temp_file_name).stat().st_size > 0)
        finally:
            # Clean up the temporary file
            Path(temp_file_name).unlink()


class TestCellPropertiesSubsetFromPath(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_image = shared_data("flower_labelled", 0)
        cls.ppty = CellProperties(cls._in_image, ["volume"], filter_neighbors=False)

    def test_nlabels(self):
        self.assertTrue(len(self.ppty.labels()) == 1040)

    def test_volume(self):
        self.assertTrue(self.ppty.volume(4) == 24373)

    def test_barycenter(self):
        np.testing.assert_almost_equal(self.ppty.barycenter(4), [160.79021868, 196.17154228, 104.64940713])

    def test_surface(self):
        self.assertTrue(self.ppty.surface(4) == 0.0)
