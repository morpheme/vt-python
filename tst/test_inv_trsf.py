import filecmp
import tempfile
import unittest
from pathlib import Path

import vt


class TestInvTrsf(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        #
        # since the first argument of vt.create_trsf are 'template_image' and 'fixedpoint_image'
        # we have to explicity set params to the variable 'params'
        #
        trsf = vt.create_trsf(params="-random")
        with tempfile.NamedTemporaryFile(delete=False) as trsf_file:
            trsf.write(trsf_file.name)
            cls._trsf = vt.vtTransformation(trsf_file.name)
            cls._trsf_file_path = trsf_file.name

    @classmethod
    def tearDownClass(cls):
        # Remove the trsf file after running all tests
        Path(cls._trsf_file_path).unlink(missing_ok=True)

    def _inv(self, params):
        # Run "structure" `inv_trsf` with given params:
        out_trsf = vt.inv_trsf(self._trsf)
        # Make sure the returned trsf is not None:
        self.assertIsNotNone(out_trsf)
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_trsf.write(py_file.name)
        del out_trsf

        # Run "command-line" `inv_trsf` with given params:
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as trsf_file:
            self._trsf.write(trsf_file.name)  # save random transformation
            cmd_status = vt.inv_trsf(trsf_file.name, params=params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "command-line" & "structure" transformation files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(cmd_file.name).unlink(missing_ok=True)
        Path(trsf_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_inv_trsf_linear(self):
        self._inv('')
