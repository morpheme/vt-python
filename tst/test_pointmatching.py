import filecmp
import tempfile
import unittest
from os.path import abspath
from os.path import dirname
from os.path import join
from pathlib import Path

import numpy as np

import vt
from vt.dataset import shared_data_path


class TestPointmatchingFromFiles(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._ref_path = abspath(join(dirname(__file__), "..", "data",
                                     "cell_barycenters_landmarks-t1.txt"))
        cls._ref_points = vt.vtPointList(cls._ref_path)
        cls._flo_path = abspath(join(dirname(__file__), "..", "data",
                                     "cell_barycenters_landmarks-t0.txt"))
        cls._flo_points = vt.vtPointList(cls._flo_path)
        cls._template_path = shared_data_path("flower_confocal_ds2x", 0)
        cls._template = vt.vtImage(str(cls._template_path))

    def _pointmatching(self, params=""):
        out_trsf = vt.pointmatching(self._flo_points, self._ref_points,
                                    self._template, params=params)
        self.assertIsNotNone(out_trsf)

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            cmd_status = vt.pointmatching(self._flo_path, self._ref_path,
                                          self._template_path, params=params,
                                          out_file_path=cmd_file.name)
            self.assertTrue(cmd_status == 0)

            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                out_trsf.write(py_file.name)
                self.assertTrue(
                    filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_pointmatching_rigid(self):
        self._pointmatching(params='-trsf-type rigid -no-verbose')

    def test_pointmatching_affine(self):
        self._pointmatching(params='-trsf-type affine -no-verbose')

    def test_pointmatching_vectorfield(self):
        self._pointmatching(params='-trsf-type vectorfield -no-verbose')

class TestPointmatchingFromArrays(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        flo_pts = [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]]
        ref_pts = [[0, 0, 0], [0, 0, 2], [0, 2, 0], [2, 0, 0]]
        cls._ref_points = vt.vtPointList(ref_pts)
        cls._flo_points = vt.vtPointList(flo_pts)
        cls._template_path = shared_data_path("flower_confocal_ds2x", 0)
        cls._template = vt.vtImage(str(cls._template_path))

    def _pointmatching(self, params=""):
        out_trsf = vt.pointmatching(self._flo_points, self._ref_points,
                                    self._template, params=params)
        self.assertIsNotNone(out_trsf)

    def test_pointmatching_rigid(self):
        self._pointmatching(params='-trsf-type rigid -no-verbose')

    def test_pointmatching_affine(self):
        self._pointmatching(params='-trsf-type affine -no-verbose')

    def test_pointmatching_vectorfield(self):
        self._pointmatching(params='-trsf-type vectorfield -no-verbose')
