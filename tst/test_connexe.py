import filecmp
import tempfile
import unittest
from pathlib import Path

from vt import connexe
from vt import regionalext
from vt import vtImage
from vt.dataset import _append_suffix
from vt.dataset import shared_data_path


class TestConnexe(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path('flower_confocal_ds2x', 0)
        cls._in_image = vtImage(str(cls._in_path))
        # Compute regional extrema (minima)
        cls._hmin_image = regionalext(cls._in_image, "-minima -connectivity 26 -h 5 -no-verbose")
        # Define the file name to save this file (in the cache)
        cls._hmin_path = _append_suffix(cls._in_path, "_hmin")
        # Save the hmin image file:
        cls._hmin_image.write(str(cls._hmin_path))

    @classmethod
    def tearDownClass(cls):
        Path(cls._hmin_path).unlink(missing_ok=True)

    def _connexe(self, params):
        # Test method with `vtImage` as input (bridge):
        out_image = connexe(self._hmin_image, params)
        # Make sure the returned `vtImage` is not `None`:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_image.write(py_file.name)
        del out_image

        # Test method with 'path to image' as input (cli):
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            # Run "command-line" with same params:
            cmd_status = connexe(self._hmin_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" image files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_connexe_hysteresis_components(self):
        self._connexe("-low-threshold 1 -high-threshold 5 -labels -connectivity 26")
