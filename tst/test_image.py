#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tempfile
import unittest
from pathlib import Path

import numpy as np

from vt.image import Image
from vt.tools import random_array


class TestImage(unittest.TestCase):

    def _setup(self, shape, dtype):
        self.shape = shape
        self.dtype = dtype
        self.ndim = len(shape)
        self.arr = random_array(shape, dtype)
        self.vxs = [x / 10. for x in range(3, 3 + self.ndim)]
        self.im = Image(self.arr, self.vxs)

    def _test_vxs(self):
        err_msg = "Voxelsizes are not equal:"
        err_msg += f"  - Known value: {self.vxs}"
        err_msg += f"  - Image: {self.im.spacing()}"
        np.testing.assert_array_almost_equal(self.vxs, self.im.spacing(),
                                             decimal=6, err_msg=err_msg)

    def _test_array_shape(self):
        err_msg = "Array shape are not equal:"
        err_msg += f"  - Known value: {self.shape}"
        err_msg += f"  - Image: {self.im.shape()}"
        np.testing.assert_array_equal(self.shape, self.im.shape(),
                                      err_msg=err_msg)

    def _test_copy_to_array_shape(self):
        err_msg = "Array shape are not equal:"
        err_msg += f"  - Known value: {self.shape}"
        err_msg += f"  - Image.copy_to_array: {self.im.copy_to_array().shape}"
        np.testing.assert_array_equal(self.shape, self.im.copy_to_array().shape,
                                      err_msg=err_msg)

    def _test_array(self):
        np.testing.assert_array_equal(self.arr, self.im.copy_to_array())

    def _test_image_write(self):
        # Create a temporary file
        with tempfile.NamedTemporaryFile(suffix=".tif", delete=False) as temp_file:
            filename = Path(temp_file.name)

        try:
            # Write the image to the temporary file
            self.im.write(str(filename))
            # Read the image back
            read_image = Image(str(filename))
            # Test if the read image's array matches the original array
            np.testing.assert_array_equal(self.arr, read_image.copy_to_array(),
                                          err_msg="The array of the written and read image do not match")
            # Test if the read image's voxel sizes match the original voxel sizes
            np.testing.assert_array_almost_equal(self.vxs, read_image.spacing(),
                                                 decimal=6,
                                                 err_msg="The voxel sizes of the written and read image do not match")
        finally:
            # Ensure the temporary file is removed
            filename.unlink()

    def test_2D_uint8(self):
        self._setup([9, 10], "uint8")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_2D_uint16(self):
        self._setup([9, 10], "uint16")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_2D_int16(self):
        self._setup([9, 10], "int16")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_3D_uint8(self):
        self._setup([9, 10, 11], "uint8")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_3D_uint16(self):
        self._setup([9, 10, 11], "uint16")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_3D_int16(self):
        self._setup([9, 10, 11], "int16")
        self._test_vxs()
        self._test_array_shape()
        self._test_copy_to_array_shape()
        self._test_array()
        self._test_image_write()

    def test_image_init_with_none(self):
        image = Image(None)
        self.assertEqual(image.spacing(), [1.0, 1.0, 1.0])
