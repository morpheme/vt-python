import filecmp
import tempfile
import unittest
from pathlib import Path

from vt import connexe
from vt import regionalext
from vt import vtImage
from vt import watershed
from vt.dataset import _append_suffix
from vt.dataset import shared_data_path


class TestWatershed(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path('flower_confocal_ds2x', 0)
        cls._in_image = vtImage(str(cls._in_path))

        h_min = 5
        cls._hmin_image = regionalext(cls._in_image, f"-minima -connectivity 26 -h {h_min} -no-verbose")
        cls._seed_image = connexe(cls._hmin_image, f"-connectivity 18 -low-threshold 1 -high-threshold {h_min} -no-verbose")
        # Define the file name to save this file (in the cache)
        cls._seed_path = _append_suffix(cls._in_path, "_seed")
        # Save the seed image file:
        cls._seed_image.write(str(cls._seed_path))

    @classmethod
    def tearDownClass(cls):
        Path(cls._seed_path).unlink(missing_ok=True)

    def _watershed(self, params):
        # Test method with `vtImage` as input:
        out_image = watershed(self._in_image, self._seed_image, params)
        # Make sure the returned image is not `None`:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_image.write(py_file.name)
        del out_image

        # Test method with 'path to image' as input:
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            # Run "command-line" with same params:
            cmd_status = watershed(self._in_path, self._seed_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" image files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_watershed_labelchoice_first(self):
        self._watershed("-labelchoice first -no-verbose")

    def test_watershed_labelchoice_min(self):
        self._watershed("-labelchoice min -no-verbose")

    def test_watershed_labelchoice_most(self):
        self._watershed("-labelchoice most -no-verbose")
