import filecmp
import tempfile
import unittest
from pathlib import Path

import vt


class TestMeanTrsfs(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    def _mean_trsfs(self, params):
        _trsfs = []
        _trsfs_path = []

        with tempfile.NamedTemporaryFile(delete=False) as trsf_file1, tempfile.NamedTemporaryFile(
                delete=False) as trsf_file2, \
                tempfile.NamedTemporaryFile(delete=False) as trsf_file3, tempfile.NamedTemporaryFile(
            delete=False) as trsf_file4, \
                tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            cmd_status = vt.create_trsf(params="-random -srandom 1", out_file_path=trsf_file1.name)
            self.assertTrue(cmd_status == 0)
            _trsfs.append(vt.vtTransformation(trsf_file1.name))
            _trsfs_path.append(trsf_file1.name)

            cmd_status = vt.create_trsf(params="-random -srandom 2", out_file_path=trsf_file2.name)
            self.assertTrue(cmd_status == 0)
            _trsfs.append(vt.vtTransformation(trsf_file2.name))
            _trsfs_path.append(trsf_file2.name)

            cmd_status = vt.create_trsf(params="-random -srandom 3", out_file_path=trsf_file3.name)
            self.assertTrue(cmd_status == 0)
            _trsfs.append(vt.vtTransformation(trsf_file3.name))
            _trsfs_path.append(trsf_file3.name)

            cmd_status = vt.create_trsf(params="-random -srandom 4", out_file_path=trsf_file4.name)
            self.assertTrue(cmd_status == 0)
            _trsfs.append(vt.vtTransformation(trsf_file4.name))
            _trsfs_path.append(trsf_file4.name)

            cmd_status = vt.mean_trsfs(_trsfs_path, params, out_file_path=cmd_file.name)
            self.assertTrue(cmd_status == 0)

            out_trsfs = vt.mean_trsfs(_trsfs, params)
            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                out_trsfs.write(py_file.name)
                self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)
        Path(trsf_file1.name).unlink(missing_ok=True)
        Path(trsf_file2.name).unlink(missing_ok=True)
        Path(trsf_file3.name).unlink(missing_ok=True)
        Path(trsf_file4.name).unlink(missing_ok=True)

    def test_mean_trsfs_mean(self):
        self._mean_trsfs("-mean -trsf-type affine")
