import filecmp
import tempfile
import unittest
from pathlib import Path

import vt
from vt.dataset import shared_data_path


class TestMeanImages(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path("flower_confocal_ds2x", 0)
        cls._images = []
        cls._images.append(vt.vtImage(str(cls._in_path)))
        cls._images.append(vt.morpho(cls._images[0], "-dil"))
        cls._images.append(vt.morpho(cls._images[0], "-ero"))

    def _mean_images(self, params):
        out_image = vt.mean_images(self._images, params=params)
        self.assertIsNotNone(out_image)

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as dil_file, \
                tempfile.NamedTemporaryFile(delete=False) as ero_file:
            cmd_status = vt.morpho(self._in_path, "-dilation", out_file_path=dil_file.name)
            self.assertTrue(cmd_status == 0)
            cmd_status = vt.morpho(self._in_path, "-erosion", out_file_path=ero_file.name)
            self.assertTrue(cmd_status == 0)
            in_paths = []
            in_paths.append(self._in_path)
            in_paths.append(dil_file.name)
            in_paths.append(ero_file.name)

            cmd_status = vt.mean_images(in_paths, params=params, out_file_path=cmd_file.name)
            self.assertTrue(cmd_status == 0)

            with tempfile.NamedTemporaryFile(delete=False) as py_file:
                out_image.write(py_file.name)
                self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(dil_file.name).unlink(missing_ok=True)
        Path(ero_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_mean_images_mean(self):
        self._mean_images("-mean")
