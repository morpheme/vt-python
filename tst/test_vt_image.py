import filecmp
import tempfile
import unittest
from pathlib import Path

import numpy as np

import vt
from vt.tools import random_array
from vt.dataset import shared_data_path


class TestVtImage(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._file_path = shared_data_path("flower_confocal_ds2x", 0)
        cls.file_image = vt.vtImage(str(cls._file_path))

        cls.rand_uint8_image_2D = random_array([17, 15], dtype='uint8')
        cls.rand_float_image_2D = random_array([17, 15], dtype='float')
        cls.rand_uint8_image_C3D = random_array([17, 15, 13], dtype='uint8')
        cls.rand_double_image_C3D = random_array([17, 15, 13], dtype='double')

    def test_vt_image_copy_to_array(self):
        """test_vt_image_copy_to_array: Make sure accessing copy_to_array always returns the same data. """
        vtim = self.file_image
        np.testing.assert_array_equal(vtim.copy_to_array(), vtim.copy_to_array())
        arr1 = vtim.copy_to_array()
        arr2 = vtim.copy_to_array()
        np.testing.assert_array_equal(arr1, arr2)

    def test_vt_image_io(self):
        """test_vt_image_io: Write a copy of a vtImage then compare original and copy files."""
        vtim = self.file_image
        with tempfile.NamedTemporaryFile(delete=False) as cp_file:
            cp_fname = cp_file.name + '.inr.gz'
            vtim.write(str(cp_fname))
            cmp = filecmp.cmp(cp_fname, self._file_path, shallow=False)
            err_msg = "Original file & its direct copy are different:\n{} != {}"
            self.assertTrue(cmp, msg=err_msg.format(self._file_path, cp_fname))
        Path(cp_file.name).unlink(missing_ok=True)

    def test_vt_image_ioi(self):
        """Write a copy of a vtImage load it again then compare original and copy arrays."""
        vtim = self.file_image
        with tempfile.NamedTemporaryFile(delete=False) as cp_file:
            cp_fname = cp_file.name + '.inr'
            vtim.write(str(cp_fname))
            cp_vtim = vt.vtImage(str(cp_fname))
            np.testing.assert_array_equal(vtim.copy_to_array(), cp_vtim.copy_to_array())
        Path(cp_file.name).unlink(missing_ok=True)

    def test_vt_image_vt2np2vt_io(self):
        """
        Copy the array of a vtImage (copy_to_array), make a new vtImage, save two
        temporary copies, the original vtImage and the vt2np2vt vtImage then
        finally compare both files (saved copies).
        """
        vtim = self.file_image
        # vt2np:
        arr = vtim.copy_to_array()
        # vt2np2vt:
        cp_vtim = vt.vtImage(arr, vtim.spacing())
        with tempfile.NamedTemporaryFile(delete=False) as ori_cp, tempfile.NamedTemporaryFile(delete=False) as vt_cp:
            ori_cp_fname = ori_cp.name + '.inr'
            vt_cp_fname = vt_cp.name + '.inr'
            vtim.write(ori_cp_fname)
            cp_vtim.write(vt_cp_fname)
            self.assertTrue(filecmp.cmp(ori_cp_fname, vt_cp_fname, shallow=False))
        Path(ori_cp.name).unlink(missing_ok=True)
        Path(vt_cp.name).unlink(missing_ok=True)

    def test_vt_image_vt2np2vt2np_io(self):
        """
        Copy the array of a vtImage (copy_to_array), make a new vtImage, save a
        temporary copy, read it with vtImage and compare original and copy arrays.
        """
        vtim = self.file_image
        # vt2np:
        arr = vtim.copy_to_array()
        # vt2np2vt:
        cp_vtim1 = vt.vtImage(arr, vtim.spacing())
        # vt2np2vt2np:
        cp_arr1 = cp_vtim1.copy_to_array()
        with tempfile.NamedTemporaryFile(delete=False) as vt_cp:
            vt_cp_fname = vt_cp.name + '.inr'
            cp_vtim1.write(vt_cp_fname)
            cp_vtim2 = vt.vtImage(str(vt_cp_fname))
            # vt2np2vt2np + IO:
            cp_arr2 = cp_vtim2.copy_to_array()
            np.testing.assert_array_equal(arr, cp_arr1)
            np.testing.assert_array_equal(arr, cp_arr2)
            np.testing.assert_array_equal(cp_arr1, cp_arr2)
        Path(vt_cp.name).unlink(missing_ok=True)

    def _copy_np_vt_np(self, a):
        i = vt.vtImage(a)
        b = i.copy_to_array()
        np.testing.assert_array_equal(a, b)

    def _copy_double_np_vt_np(self, a):
        c = a
        i = vt.vtImage(a)
        # i.foo(c)
        # j = vt.vtImage(c)
        # i.foo(c)
        b = i.copy_to_array()
        np.testing.assert_array_equal(a, b)

    def test_vt_image_copy_nparray_uint8_2D(self):
        self._copy_np_vt_np(self.rand_uint8_image_2D)

    def test_vt_image_copy_nparray_float_2D(self):
        self._copy_np_vt_np(self.rand_float_image_2D)

    # def test_vt_image_copy_nparray_uint8_F3D(self):
    #    self._copy_np_vt_np(self.rand_uint8_image_F3D)

    # def test_vt_image_copy_nparray_double_F3D(self):
    #    self._copy_double_np_vt_np(self.rand_double_image_F3D)

    def test_vt_image_copy_nparray_uint8_C3D(self):
        self._copy_np_vt_np(self.rand_uint8_image_C3D)

    def test_vt_image_copy_nparray_double_C3D(self):
        self._copy_double_np_vt_np(self.rand_double_image_C3D)

    def _copy2_np_vt_np(self, a):
        i = vt.vtImage(a)
        c = i.copy_to_array()
        b = np.array(i)
        np.testing.assert_equal(a, b)

    def test_copy2_vt_image_nparray_uint8_2D(self):
        self._copy2_np_vt_np(self.rand_uint8_image_2D)

    def test_copy2_vt_image_nparray_float_2D(self):
        self._copy2_np_vt_np(self.rand_float_image_2D)

    def test_copy2_vt_image_nparray_uint8_C3D(self):
        self._copy2_np_vt_np(self.rand_uint8_image_C3D)

    def test_copy2_vt_image_nparray_double_C3D(self):
        self._copy2_np_vt_np(self.rand_double_image_C3D)


#
#
#

class AbstractTestVIimage(object):
    _data = None
    _spacing = [0.1, 0.2, 0.3]

    def _from_numpy(self):
        return vt.vtImage(self._data, self._spacing)

    def _check_numpy(self, vt_image):
        for index, value in enumerate(vt_image.spacing()):
            self.assertAlmostEqual(value, self._spacing[index])
        self.assertTrue(np.array_equal(vt_image.copy_to_array(), self._data))

    def test_vt_image_from_numpy(self):
        vt_image = self._from_numpy()
        self._check_numpy(vt_image)

    def test_vt_image_io(self):
        in_image = self._from_numpy()
        with tempfile.NamedTemporaryFile(delete=False) as out_file:
            in_image.write(out_file.name)
            out_image = vt.vtImage(out_file.name)
        self._check_numpy(out_image)
        in_arr = in_image.copy_to_array()
        out_arr = out_image.copy_to_array()
        np.testing.assert_array_equal(in_arr, out_arr)
        self.assertTrue(in_arr.dtype == out_arr.dtype)
        self.assertTrue(self._data.dtype == out_arr.dtype)
        Path(out_file.name).unlink(missing_ok=True)


class TestVTImageShort(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 0, 0]],
                              [[2, 0, 2, 0],
                               [0, 0, 0, 0],
                               [2, 0, 2, 0]],
                              [[0, 3, 0, 0],
                               [3, 0, 3, 0],
                               [0, 3, 0, 0]]], dtype=np.short)


class TestVTImageInt(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 0, 0]],
                              [[2, 0, 2, 0],
                               [0, 0, 0, 0],
                               [2, 0, 2, 0]],
                              [[0, 3, 0, 0],
                               [3, 0, 3, 0],
                               [0, 3, 0, 0]]], dtype=np.int32)


class TestVTImageDouble(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 0, 0]],
                              [[2, 0, 2, 0],
                               [0, 0, 0, 0],
                               [2, 0, 2, 0]],
                              [[0, 3, 0, 0],
                               [3, 0, 3, 0],
                               [0, 3, 0, 0]]], dtype=np.float64)


class TestVTImageFloat(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 0, 0]],
                              [[2, 0, 2, 0],
                               [0, 0, 0, 0],
                               [2, 0, 2, 0]],
                              [[0, 3, 0, 0],
                               [3, 0, 3, 0],
                               [0, 3, 0, 0]]], dtype=float)


class TestVTImageChar(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[["0", "0", "0", "0"],
                               ["0", "1", "0", "0"],
                               ["0", "0", "0", "0"]],
                              [["2", "0", "2", "0"],
                               ["0", "0", "0", "0"],
                               ["2", "0", "2", "0"]],
                              [["0", "3", "0", "0"],
                               ["3", "0", "3", "0"],
                               ["0", "3", "0", "0"]]], dtype=np.byte)


class TestVTImageUnsignedChar(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[["0", "0", "0", "0"],
                               ["0", "1", "0", "0"],
                               ["0", "0", "0", "0"]],
                              [["2", "0", "2", "0"],
                               ["0", "0", "0", "0"],
                               ["2", "0", "2", "0"]],
                              [["0", "3", "0", "0"],
                               ["3", "0", "3", "0"],
                               ["0", "3", "0", "0"]]], dtype=np.ubyte)


class TestVTImageUnsignedShort(AbstractTestVIimage, unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._data = np.array([[[0, 0, 0, 0],
                               [0, 1, 0, 0],
                               [0, 0, 0, 0]],
                              [[2, 0, 2, 0],
                               [0, 0, 0, 0],
                               [2, 0, 2, 0]],
                              [[0, 3, 0, 0],
                               [3, 0, 3, 0],
                               [0, 3, 0, 0]]], dtype=np.ushort)
