import filecmp
import os
import tempfile
import unittest
from pathlib import Path

from vt import apply_trsf_to_points
from vt import pointmatching
from vt import vtPointList
from vt import vtTransformation


class TestApplyTrsfToPoints(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._ref_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "data",
                                                     "cell_barycenters_landmarks-t1.txt"))
        cls._ref_points = vtPointList(cls._ref_path)
        cls._flo_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "data",
                                                     "cell_barycenters_landmarks-t0.txt"))
        cls._flo_points = vtPointList(cls._flo_path)

    def _test_apply_trsf_to_points_rigid(self, pointmatching_params=""):
        # Test method with `vtPointList` as input:
        out_trsf = pointmatching(self._flo_points, self._ref_points, params=pointmatching_params)
        # Make sure the returned `vtTransformation` is not `None`:
        self.assertIsNotNone(out_trsf)

        with tempfile.NamedTemporaryFile(delete=False) as cmd_file, tempfile.NamedTemporaryFile(
                delete=False) as trsf_file:
            #
            # transformation is written and re-read to avoid rounding effects
            #
            out_trsf.write(trsf_file.name)
            res_trsf = vtTransformation(trsf_file.name)

            res_points = apply_trsf_to_points(self._flo_points, res_trsf, params="")

            cmd_status = apply_trsf_to_points(self._flo_path, trsf_file.name, params="", out_file_path=cmd_file.name)
            self.assertTrue(cmd_status == 0)
            # shutil.copyfile(cmd_file.name,"/Users/greg/TMP/cmd.txt")

        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            res_points.write(py_file.name)
            # res_points.write("/Users/greg/TMP/py.txt")
            self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        Path(py_file.name).unlink(missing_ok=True)
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(trsf_file.name).unlink(missing_ok=True)

    def test_apply_trsf_to_points_rigid(self):
        self._test_apply_trsf_to_points_rigid(pointmatching_params='-trsf-type rigid -no-verbose')
