import filecmp
import tempfile
import unittest
from pathlib import Path

import vt
from vt.dataset import shared_data_path


class TestLinearFilter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path("flower_confocal", 0)
        cls._in_image = vt.vtImage(str(cls._in_path))

    def _linear_filter(self, params):
        # Test method with `vtImage` as input (bridge):
        out_image = vt.linear_filter(self._in_image, params)
        # Make sure the returned `vtImage` is not `None`:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_image.write(py_file.name)

        # Test method with 'path to image' as input (cli):
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            cmd_status = vt.linear_filter(self._in_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_linear_filter_smoothing(self):
        self._linear_filter("-smoothing -sigma 1.0 -unit real")

    def test_linear_filter_laplacian(self):
        self._linear_filter("-laplacian -sigma 1.0 -unit real")
