import filecmp
import os
import random
import tempfile
import unittest
from pathlib import Path

import numpy as np
from vt.tools import random_array

import vt


class TestVtPointList(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._points_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "data",
                                                        "cell_barycenters_landmarks-t0.txt"))
        cls.file_points = vt.vtPointList(cls._points_path)
        # Create two random arrays
        cls.rand_double_points = random_array(dtype='double', shape=[12, 3])
        cls.rand_float_points = random_array(dtype='float', shape=[15, 3])

    def _vt_np_vt(self, pts):
        a = pts.copy_to_array()
        l = vt.vtPointList(a)
        # Test unit default value:
        self.assertEqual(l.unit(), "real")
        # Test spacing default value:
        np.testing.assert_array_equal(l.spacing(), [1., 1., 1.])
        # Save file from `vtPointList` and compare to original:
        with tempfile.NamedTemporaryFile(delete=False) as pts_file, tempfile.NamedTemporaryFile(
                delete=False) as array_file:
            pts.write(pts_file.name)
            l.write(array_file.name)
            self.assertTrue(filecmp.cmp(pts_file.name, array_file.name, shallow=False))
        Path(pts_file.name).unlink(missing_ok=True)
        Path(array_file.name).unlink(missing_ok=True)

    def _np_vt_np(self, a):
        pts = vt.vtPointList(a)
        # Test unit default value:
        self.assertEqual(pts.unit(), "real")
        # Test spacing default value:
        np.testing.assert_array_equal(pts.spacing(), [1., 1., 1.])
        # Copy array of point and compare to original
        b = pts.copy_to_array()
        self.assertTrue((a == b).all())

    def test_vt_pointlist_file(self):
        self._vt_np_vt(self.file_points)

    def test_vt_pointlist_nparray_double(self):
        self._np_vt_np(self.rand_double_points)

    def test_vt_pointlist_nparray_float(self):
        self._np_vt_np(self.rand_float_points)
