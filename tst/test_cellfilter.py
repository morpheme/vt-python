import filecmp
import tempfile
import unittest
from pathlib import Path

from vt import cellfilter
from vt import vtImage
from vt.dataset import shared_data_path


class TestCellfilter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path('flower_labelled', 0)
        cls._in_image = vtImage(str(cls._in_path))

    def _cellfilter(self, params):
        # Test method with `vtImage` as input (bridge):
        out_image = cellfilter(self._in_image, params)
        # Make sure the returned `vtImage` is not `None`:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            out_image.write(py_file.name)
        del out_image

        # Test method with 'path to image' as input (cli):
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            # Run "command-line" `blockmatching` with same params:
            cmd_status = cellfilter(self._in_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "bridge" & "cli" files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_cellfilter_opening_sphere(self):
        self._cellfilter("-operation opening -R 2 -no-verbose")

    def test_cellfilter_closing_sphere(self):
        self._cellfilter("-operation closing -R 2 -no-verbose")

    def test_cellfilter_erosion_sphere(self):
        self._cellfilter("-operation erosion -R 2 -no-verbose")

    def test_cellfilter_dilation_sphere(self):
        self._cellfilter("-operation dilation -R 2 -no-verbose")
