import filecmp
import tempfile
import unittest
from pathlib import Path

import vt
from vt.dataset import shared_data_path


class TestRegionalext(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls._in_path = shared_data_path('flower_confocal_ds2x', 0)
        cls._in_image = vt.vtImage(str(cls._in_path))

    def _regionalext(self, params):
        # Test method with `vtImage` as input:
        out_image = vt.regionalext(self._in_image, params)
        # Make sure the returned image is not None:
        self.assertIsNotNone(out_image)
        # Save the output image:
        with tempfile.NamedTemporaryFile(delete=False) as py_file:
            # Write the "structure" image file:
            out_image.write(py_file.name)

        # Test method with 'path to image' as input:
        with tempfile.NamedTemporaryFile(delete=False) as cmd_file:
            # Run "command-line" with same params:
            cmd_status = vt.regionalext(self._in_path, params, out_file_path=cmd_file.name)
            # Assert "command-line" call was properly executed:
            self.assertTrue(cmd_status == 0)

        # Compare "command-line" & "structure" image files:
        self.assertTrue(filecmp.cmp(cmd_file.name, py_file.name, shallow=False))

        # Clean up temporary files:
        Path(cmd_file.name).unlink(missing_ok=True)
        Path(py_file.name).unlink(missing_ok=True)

    def test_regionalext_maxima26(self):
        self._regionalext("-maxima -connectivity 26 -h 5 -no-verbose")

    def test_regionalext_minima26(self):
        self._regionalext("-minima -connectivity 26 -h 5 -no-verbose")

    def test_regionalext_minima18(self):
        self._regionalext("-minima -connectivity 18 -h 5 -no-verbose")

    def test_regionalext_minima6(self):
        self._regionalext("-minima -connectivity 6 -h 5 -no-verbose")
