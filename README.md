# VT-python library

This package contains Python wrappers for the C library [`vt`](https://gitlab.inria.fr/morpheme/vt).

It uses `conda` to create environments and install dependencies.
If you don't have `conda` installed, follow the instructions [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).

## User Installation

To install the `vt-python` library and its dependencies in a new environment named `morpheme`, use the following command:

```bash
conda create -n morpheme vt-python -c morpheme
```

Note: `-c morpheme` specifies the morpheme channel on [Anaconda Cloud](https://anaconda.org/).

To install additional packages like `ipython`:

```bash
conda install -n morpheme ipython
```

Usage example:

```python
import vt

img = vt.vtImage("my_image.tif")
```

Replace `my_image.tif` with a valid image.

## Developer Installation

Choose between installing the `vt` C library from source or using the conda package.

### Option A: Install `vt` from source

1. Follow the `vt` source installation instructions [here](https://gitlab.inria.fr/morpheme/vt).
2. Update the `morpheme_dev` environment with `vt-python` dependencies:

```bash
conda env update -n morpheme_dev --file pkg/env/vt-python-dev.yaml
```

### Option B: Install `vt` conda package

Create a new environment using the `vt-python` recipe:

```bash
conda env create -n morpheme_dev -f pkg/env/vt-python.yaml
```

### Install `vt-python` source

1. Clone the repository:

```bash
git clone https://gitlab.inria.fr/morpheme/vt-python.git
```

2. Install `vt-python` in editable mode:

```bash
conda activate morpheme_dev
cd vt-python
rm -rf build  # Clean previous build directory if it exists
python -m pip install -e .
```

## Contributing

### Building and Uploading Conda Package

**Important:** Perform these steps from the `base` environment.

1. Install required tools:

```bash
conda activate base
conda install conda-build anaconda-client
```

For macOS, follow the [macOS SDK installation instructions](https://docs.conda.io/projects/conda-build/en/latest/resources/compiler-tools.html#macos-sdk).

2. Build the conda package:

**Important:** The following command will build conda packages for a number of Python version, 3.8 to 3.12.

```bash
cd vt-python
conda build -c morpheme -c conda-forge pkg/recipe/.
```

To build for a specific Python version (_e.g._, 3.8), add `--python=3.8` to the command.

3. (Optional) Upload to Anaconda Cloud:

Follow [this procedure](https://docs.conda.io/projects/conda-build/en/latest/user-guide/tutorials/build-pkgs-skeleton.html#optional-uploading-packages-to-anaconda-org) to set up your Anaconda Cloud account.

To build and automatically upload:

```bash
conda build -c conda-forge -c morpheme . --user morpheme
```

Replace `morpheme` with your Anaconda Cloud username or organization.

### Running Tests

To run tests, install `nose` and `coverage`:

```bash
conda install nose coverage -n morpheme
```

Run tests from the `tst` directory:

```bash
cd tst
nosetests -v
```