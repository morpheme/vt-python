# setup.py ---

from setuptools import setup
from cmake.cmake_build import CMakeExtension

setup(ext_modules=[CMakeExtension('vt.vtpython')])

#
# setup.py ends here
