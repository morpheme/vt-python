import shutil
import sys


def format_bytes(bytes_value):
    """Convert bytes to the most appropriate unit."""
    units = ['bytes', 'KB', 'MB', 'GB', 'TB']
    unit_index = 0
    value = float(bytes_value)

    while value >= 1024 and unit_index < len(units) - 1:
        value /= 1024
        unit_index += 1

    return f"{value:.2f} {units[unit_index]}"


def print_disk_usage(path):
    """Try to print the disk usage statistics from a given path."""
    try:
        total, used, free = shutil.disk_usage(path)
    except FileNotFoundError:
        print(f"Error: The path '{path}' was not found.")
    except PermissionError:
        print(f"Error: Permission denied for path '{path}'.")
    else:
        print(f"Disk usage statistics from path '{path}':")
        print(f"Total: {format_bytes(total)}")
        print(f"Used:  {format_bytes(used)}")
        print(f"Free:  {format_bytes(free)}")


if __name__ == "__main__":
    print_disk_usage(sys.argv[1])
