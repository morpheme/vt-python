#!/usr/bin/env bash

if [[ -d build ]]
then 
  rm -rf build
fi

${PYTHON} -m pip install -v .
