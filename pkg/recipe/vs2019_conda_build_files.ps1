# Define file paths
$condaConfigPath = "pkg\recipe\conda_build_config.yaml"

# Replace "vs2022" with "vs2019" in 'conda_build_config.yaml'
if (Test-Path $condaConfigPath) {
    (Get-Content $condaConfigPath) -replace 'vs2022', 'vs2019' | Set-Content $condaConfigPath
    Write-Host "Replaced 'vs2022' with 'vs2019' in $condaConfigPath"
} else {
    Write-Warning "File $condaConfigPath not found"
}
