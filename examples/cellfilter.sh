#!/bin/bash

DATADIR=../data
RESDIR=result-cellfilter

mkdir -p ${RESDIR}

#
# opening
#
cellfilter ${DATADIR}/p58-t0_SEG_down_interp_2x.inr \
	     ${RESDIR}/bash-cellfilter.mha -operation opening -R 2 -no-verbose
