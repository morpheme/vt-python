import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-blockmatching"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with file names
#

vt.blockmatching(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
                 os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
                 params="-trsf-type affine -py-ll 2",
                 out_file_path=os.path.join(RESDIR, "py-filename-transfo-affine.trsf"))


vt.apply_trsf(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
              os.path.join(RESDIR, "py-filename-transfo-affine.trsf"),
              ref=os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
              out_file_path=os.path.join(RESDIR, "py-filename-floating-after-affine.mha"))


vt.blockmatching(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
                 os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
                 trsf_left=os.path.join(RESDIR, "py-filename-transfo-affine.trsf"),
                 params="-trsf-type vectorfield -py-ll 2",
                 out_file_path=os.path.join(RESDIR, "py-filename-transfo-deform.trsf"))


vt.compose_trsf([os.path.join(RESDIR, "py-filename-transfo-affine.trsf"),
                 os.path.join(RESDIR, "py-filename-transfo-deform.trsf")],
                out_file_path=os.path.join(RESDIR, "py-filename-transfo-composition.trsf"))


vt.apply_trsf(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
              os.path.join(RESDIR, "py-filename-transfo-composition.trsf"),
              ref=os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
              out_file_path=os.path.join(RESDIR, "py-filename-floating-after-composition.mha"))

#
#
#

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-affine.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-transfo-affine.trsf"),
                    os.path.join(RESDIR, "bash-transfo-affine.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-floating-after-affine.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-floating-after-affine.mha"),
                    os.path.join(RESDIR, "bash-floating-after-affine.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-deform.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-transfo-deform.trsf"),
                    os.path.join(RESDIR, "bash-transfo-deform.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": vectorfield transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-composition.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-transfo-composition.trsf"),
                    os.path.join(RESDIR, "bash-transfo-composition.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": composition transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-floating-after-composition.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-floating-after-composition.mha"),
                    os.path.join(RESDIR, "bash-floating-after-composition.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after composition transformation, comparison with bash result is " + str(c))
