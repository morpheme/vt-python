import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-blockmatching"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with python structures
#

image_flo = vt.vtImage(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"))
image_ref = vt.vtImage(os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"))

trsf_affine = vt.blockmatching(image_flo, image_ref, params="-trsf-type affine -py-ll 2")

#
# we write and re-read the transformation to have the same float truncature
# else all tests (apart the first one) will be False
#
# THIS IS ONLY FOR TEST PURPOSE!
#
trsf_affine.write(os.path.join(RESDIR, "py-structure-transfo-affine.trsf"))
del trsf_affine
trsf_affine = vt.vtTransformation(os.path.join(RESDIR, "py-structure-transfo-affine.trsf"))


image_affine = vt.apply_trsf(image_flo, trsf_affine, ref=image_ref)

trsf_deform = vt.blockmatching(image_flo, image_ref, trsf_left=trsf_affine, params="-trsf-type vectorfield -py-ll 2")

trsf_composition = vt.compose_trsf([trsf_affine, trsf_deform])

image_composition = vt.apply_trsf(image_flo, trsf_composition, ref=image_ref)

#
#
#

trsf_affine.write(os.path.join(RESDIR, "py-structure-transfo-affine.trsf"))
image_affine.write(os.path.join(RESDIR, "py-structure-floating-after-affine.mha"))
trsf_deform.write(os.path.join(RESDIR, "py-structure-transfo-deform.trsf"))
trsf_composition.write(os.path.join(RESDIR, "py-structure-transfo-composition.trsf"))
image_composition.write(os.path.join(RESDIR, "py-structure-floating-after-composition.mha"))

#
#
#

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-affine.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-transfo-affine.trsf"),
                    os.path.join(RESDIR, "bash-transfo-affine.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-floating-after-affine.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-floating-after-affine.mha"),
                    os.path.join(RESDIR, "bash-floating-after-affine.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-deform.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-transfo-deform.trsf"),
                    os.path.join(RESDIR, "bash-transfo-deform.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": vectorfield transformation, comparison with bash result is " + str(c))

#
# This next test is False: it appears that a few values have a difference of an order of magnitude of 10^{-6}
#

if os.path.isfile(os.path.join(RESDIR, "bash-transfo-composition.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-transfo-composition.trsf"),
                    os.path.join(RESDIR, "bash-transfo-composition.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": composition transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-floating-after-composition.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-floating-after-composition.mha"),
                    os.path.join(RESDIR, "bash-floating-after-composition.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after composition transformation, comparison with bash result is " + str(c))
