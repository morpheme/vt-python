import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR="../data"
RESDIR="result-linear-filter"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with python structures
# smoothing
#

image_in = vt.vtImage(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"))
image_out = vt.linear_filter(image_in, "-smoothing -sigma 1.0 -unit real")
image_out.write(os.path.join(RESDIR,"py-structure-smoothing.mha"))

del image_in
del image_out

if os.path.isfile(os.path.join(RESDIR,"bash-smoothing.mha")):
    c = filecmp.cmp(os.path.join(RESDIR,"py-structure-smoothing.mha"), os.path.join(RESDIR,"bash-smoothing.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": smoothing, comparison with bash result is " + str(c) )

#
# example with file names
# laplacian
#

image_in = vt.vtImage(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"))
image_out = vt.linear_filter(image_in, "-laplacian -sigma 1.0 -unit real")
image_out.write(os.path.join(RESDIR,"py-structure-laplacian.mha"))

del image_in
del image_out

if os.path.isfile(os.path.join(RESDIR,"bash-laplacian.mha")):
    c = filecmp.cmp(os.path.join(RESDIR,"py-structure-laplacian.mha"), os.path.join(RESDIR,"bash-laplacian.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": laplacian, comparison with bash result is " + str(c) )

