# -*- python -*-
# -*- coding: utf-8 -*-

import numpy as np
import os
from vt import vtImage


def plot_2D_array(arr, cmap='gray', order='ZYX', title=""):
    """Plot a 2D array.

    Parameters
    ----------
    arr : np.ndarray
        The array to represent.
    cmap : str in {'gray', 'viridis'}, optional
        Colormap name, all those known by matplolib works. By default use 'gray'.
    title :

    """

    if order is 'ZYX':
        import matplotlib.pyplot as plt
        plt.figure()
        plt.imshow(arr, cmap=cmap)
        ltitle = title + " - ZYX-ordered"
    elif order is 'XYZ':
        import matplotlib.pyplot as plt
        plt.figure()
        plt.imshow(np.transpose(arr), cmap=cmap)
        ltitle = title + " - XYZ-ordered"
    else:
        print("do not know how to plot image")
        return

    plt.suptitle(ltitle)

    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')

    plt.colorbar()
    plt.show()


def plot_3D_array_z_slice(arr, z_slice=None, cmap='gray', order='ZYX', title=""):
    """Plot a z-slice of a given 3D array.

    Parameters
    ----------
    arr : np.ndarray
        The array to represent.
    z_slice : int, optional
        The index of the z-sclice to represent.
    cmap : str in {'gray', 'viridis'}, optional
        Colormap name, all those known by matplolib works. By default use 'gray'.
    title :

    """

    if arr.ndim <= 1:
        print("not an image")
    elif arr.ndim == 2:
        plot_2D_array(arr, cmap=cmap, order=order, title=title)
    elif arr.ndim == 3:
        if order is 'ZYX':
            if z_slice is None:
                z_slice = arr.shape[0] // 2
            extarr = arr[z_slice, :, :]
        elif order is 'XYZ':
            if z_slice is None:
                z_slice = arr.shape[2] // 2
            extarr = arr[:, :, z_slice]
        else:
            print("do not know how to plot image")
            return
        plot_2D_array(extarr, cmap=cmap, order=order, title=title+" - "+str(z_slice)+"-slice")
    else:
        print("not an image")




# ------------------------------------------------------------------------------
# Test plotting function with a handmade 3D example:
# ------------------------------------------------------------------------------
def _plot_handmade_3D(order='XYZ'):

    xy_shape = 10
    mid_xy = xy_shape//2
    z_shape = 5

    if order is 'XYZ':
        # - Create a 2D array with a diagonal at max value (255):
        arr_2d = np.diag(np.repeat(255, xy_shape))
        # - For all rows at middle column, replace by 200:
        arr_2d[:, mid_xy] = 200
        # - For all columns at middle row, replace by 100:
        arr_2d[mid_xy, :] = 100
        # - Repeat this 2D array `z_shape` times along new dimension to get a 3D array:
        arr_3d = np.repeat(arr_2d[:, :, np.newaxis], z_shape, axis=2)
        lorder=order
    else:
        arr_2d = np.diag(np.repeat(255, xy_shape))
        arr_2d[mid_xy, :] = 200
        arr_2d[:, mid_xy] = 100
        arr_3d = np.repeat(arr_2d[np.newaxis, :, :], z_shape, axis=0)
        lorder = 'ZYX'


    # print("2D array is C-contiguous:", arr_2d.flags.c_contiguous)
    # print("3D array is C-contiguous:", arr_3d.flags.c_contiguous)
    # print("Z-slice of 3D array is C-contiguous:", arr_3d[:, :, 1].flags.c_contiguous)
    # print("Z-slice copy of 3D array is C-contiguous:", arr_3d[:, :, 1].copy().flags.c_contiguous)

    print("- type q to quit")

    plot_3D_array_z_slice(arr_3d, cmap='viridis', order=order, title='Test array')


# ------------------------------------------------------------------------------
# Use plotting function with real data:
# ------------------------------------------------------------------------------

def _plot_example_data(img_name, order='ZYX'):

    # Defines the image to read:
    img_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "data", img_name))

    # Read this file with vt.vtImage:
    vt_im = vtImage(img_path)
    sh = vt_im.shape()
    print("Image shape is: {}".format(sh))
    # Extract the array of data:
    arr = vt_im.copy_to_array().copy()
    print("Array shape is: {}".format(arr.shape))
    print("- type q to quit")

    plot_3D_array_z_slice(arr, cmap='gray', order=order, title=img_name)

# ------------------------------------------------------------------------------
#
# ------------------------------------------------------------------------------

def _print_array_information(arr):
    print("   - Array shape is: {}".format(arr.shape))
    print("   - Array strides is: {}".format(arr.strides))
    print("   - Array flags are: {}".format(arr.flags))
    print("   - Array data is: {}".format(arr.data))
    print("")

def _build_array(x, y, z=1, dtype=np.ubyte, order='ZYX'):
    a = None
    if order is 'ZYX':
        if z == 1:
            a = np.ndarray([y, x], dtype=dtype)
            for y in range(a.shape[0]):
                for x in range(a.shape[1]):
                    a[y][x] = y * a.shape[1] + x
        elif z > 1:
            a = np.ndarray([z, y, x], dtype=dtype)
            for z in range(a.shape[0]):
                for y in range(a.shape[1]):
                    for x in range(a.shape[2]):
                        a[z][y][x] = z * a.shape[1] * a.shape[2] + y * a.shape[2] + x
    else:
        if z == 1:
            a = np.ndarray([x, y], dtype=dtype)
            for y in range(a.shape[1]):
                for x in range(a.shape[0]):
                    a[x][y] = y * a.shape[0] + x
        elif z > 1:
            a = np.ndarray([x, y, z], dtype=dtype)
            for z in range(a.shape[2]):
                for y in range(a.shape[1]):
                    for x in range(a.shape[0]):
                        a[x][y][z] = z * a.shape[1] * a.shape[0] + y * a.shape[0] + x

    return a


def _plot_build_array(x, y, z=1, dtype=np.ubyte, order='ZYX'):
    arr = _build_array(x=x, y=y, z=z, dtype=dtype, order=order)
    if z > 1:
        plot_3D_array_z_slice(arr, cmap='gray', order=order)
    else:
        plot_2D_array(arr, cmap='gray', order=order)


def _XYZ_ZYX_convert(arr):
    if arr.ndim <= 1:
        print("not an image")
    elif arr.ndim == 2:
        return arr.transpose([1,0])
    elif arr.ndim == 3:
        return arr.transpose([2, 1, 0])
    else:
        print("not an image")


def _test_plot_build_array(x, y, z=1, dtype=np.ubyte):

    print("1. build a 'ZYX'-array")
    arr = _build_array(x=x, y=y, z=z, dtype=dtype, order='ZYX')
    _print_array_information(arr)
    plot_3D_array_z_slice(arr, cmap='gray', order='ZYX')

    print("2. convert it into a 'XYZ'-array")
    brr = _XYZ_ZYX_convert(arr)
    _print_array_information(brr)
    plot_3D_array_z_slice(brr, cmap='gray', order='XYZ')

    print("3. back to a 'ZYX'-array")
    crr = _XYZ_ZYX_convert(brr)
    _print_array_information(crr)
    plot_3D_array_z_slice(crr, cmap='gray', order='ZYX')


# ------------------------------------------------------------------------------
# main
# ------------------------------------------------------------------------------
_plot_handmade_3D()
_plot_handmade_3D(order='ZYX')
_plot_example_data('p58-t0_INT_down_interp_2x.inr')

# _plot_build_array(19, 17, 13)
# _plot_build_array(19, 17, 13, order='XYZ')

_test_plot_build_array(9, 7, 5)
