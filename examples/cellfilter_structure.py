import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR="../data"
RESDIR="result-cellfilter"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with python structures
#

image_in = vt.vtImage(os.path.join(DATADIR, "p58-t0_SEG_down_interp_2x.inr"))
image_out = vt.cellfilter(image_in, "-operation opening -R 2 -no-verbose")
image_out.write(os.path.join(RESDIR,"py-structure-cellfilter.mha"))

del image_in
del image_out

if os.path.isfile(os.path.join(RESDIR,"bash-cellfilter.mha")):
    c = filecmp.cmp(os.path.join(RESDIR,"py-structure-cellfilter.mha"), os.path.join(RESDIR,"bash-cellfilter.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": comparison with bash result is " + str(c) )
