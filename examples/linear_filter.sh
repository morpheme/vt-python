#!/bin/bash

DATADIR=../data
RESDIR=result-linear-filter

mkdir -p ${RESDIR}

#
# smoothing
#
linearFilter ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	     ${RESDIR}/bash-smoothing.mha -smoothing -sigma 1.0 -unit real


#
# laplacian
#
linearFilter ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	     ${RESDIR}/bash-laplacian.mha -laplacian -sigma 1.0 -unit real

