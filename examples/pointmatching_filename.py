import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-pointmatching"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)
    
#
# pointmatching computes a transformation that allows to resample
# the floating image from which the floating points are drawn
# -> Thus it goes from the reference image to the floating ones
# -> Thus we can transform the reference points
#

#
# example with file names
#

vt.pointmatching(os.path.join(DATADIR, "cell_barycenters_landmarks-t0.txt"),
                 os.path.join(DATADIR, "cell_barycenters_landmarks-t1.txt"),
                 params="-trsf-type rigid -estimator-type lts",
                 out_file_path=os.path.join(RESDIR, "py-filename-trsf-rigid-t1-t0.trsf"))

vt.apply_trsf(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
              os.path.join(RESDIR, "py-filename-trsf-rigid-t1-t0.trsf"),
              ref=os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
              out_file_path=os.path.join(RESDIR, "py-filename-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"))

vt.apply_trsf_to_points(os.path.join(DATADIR, "cell_barycenters_landmarks-t1.txt"),
              os.path.join(RESDIR, "py-filename-trsf-rigid-t1-t0.trsf"),
              out_file_path=os.path.join(RESDIR, "py-filename-cell_barycenters_landmarks-t1-rigid.txt"))

#
#
#
params = "-trsf-type vectorfield -vector-propagation-type skiz -vector-fading-distance 15"
params += " -vector-propagation-distance 20 -estimator-type lts -lts-iterations 5"

vt.pointmatching(os.path.join(DATADIR, "cell_barycenters_landmarks-t0.txt"),
                 os.path.join(DATADIR, "cell_barycenters_landmarks-t1.txt"),
                 ref=os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
                 params=params,
                 out_file_path=os.path.join(RESDIR, "py-filename-trsf-vectorfield-t1-t0.trsf"))

vt.apply_trsf(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"),
              os.path.join(RESDIR, "py-filename-trsf-vectorfield-t1-t0.trsf"),
              ref=os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"),
              out_file_path=os.path.join(RESDIR, "py-filename-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"))

vt.apply_trsf_to_points(os.path.join(DATADIR, "cell_barycenters_landmarks-t1.txt"),
              os.path.join(RESDIR, "py-filename-trsf-vectorfield-t1-t0.trsf"),
              out_file_path=os.path.join(RESDIR, "py-filename-cell_barycenters_landmarks-t1-vectorfield.txt"))




#
#
#

if os.path.isfile(os.path.join(RESDIR, "bash-trsf-rigid-t1-t0.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-trsf-rigid-t1-t0.trsf"),
                    os.path.join(RESDIR, "bash-trsf-rigid-t1-t0.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": rigid transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-p58-t0-in-t1-rigid_INT_down_interp_2x.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"),
                    os.path.join(RESDIR, "bash-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-rigid.txt")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-cell_barycenters_landmarks-t1-rigid.txt"),
                    os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-rigid.txt"),
                    shallow=False)
    print(" - " + str(__file__) + ": points after linear transformation, comparison with bash result is " + str(c))
    
if os.path.isfile(os.path.join(RESDIR, "bash-trsf-vectorfield-t1-t0.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-trsf-vectorfield-t1-t0.trsf"),
                    os.path.join(RESDIR, "bash-trsf-vectorfield-t1-t0.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": vectorfield transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"),
                    os.path.join(RESDIR, "bash-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after vectorfield transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-vectorfield.txt")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-cell_barycenters_landmarks-t1-vectorfield.txt"),
                    os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-vectorfield.txt"),
                    shallow=False)
    print(" - " + str(__file__) + ": points after vectorfield transformation, comparison with bash result is " + str(c))
