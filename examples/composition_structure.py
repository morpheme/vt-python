import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-blockmatching"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with python structures
#

image_flo = vt.vtImage(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"))
image_ref = vt.vtImage(os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"))

trsf_affine = vt.blockmatching(image_flo, image_ref, params="-trsf-type affine -py-ll 2")
trsf_deform = vt.blockmatching(image_flo, image_ref, trsf_left=trsf_affine, params="-trsf-type vectorfield -py-ll 2")

trsf_comp01 = vt.compose_trsf([trsf_affine, trsf_deform])

image_flo = vt.vtImage(os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"))
image_ref = vt.vtImage(os.path.join(DATADIR, "p58-t2_INT_down_interp_2x.inr"))

trsf_affine = vt.blockmatching(image_flo, image_ref, params="-trsf-type affine -py-ll 2")
trsf_deform = vt.blockmatching(image_flo, image_ref, trsf_left=trsf_affine, params="-trsf-type vectorfield -py-ll 2")

trsf_comp12 = vt.compose_trsf([trsf_affine, trsf_deform])

trsf_comp02 = vt.compose_trsf([trsf_comp01, trsf_comp12])
