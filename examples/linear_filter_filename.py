import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-linear-filter"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with file names
# smoothing
#

vt.linear_filter(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"), "-smoothing -sigma 1.0 -unit real",
                 out_file_path=os.path.join(RESDIR, "py-filename-smoothing.mha"))

if os.path.isfile(os.path.join(RESDIR, "bash-smoothing.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-smoothing.mha"), os.path.join(RESDIR, "bash-smoothing.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": smoothing, comparison with bash result is " + str(c))

#
# example with file names
# laplacian
#

vt.linear_filter(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"), "-laplacian -sigma 1.0 -unit real",
                 out_file_path=os.path.join(RESDIR, "py-filename-laplacian.mha"))

if os.path.isfile(os.path.join(RESDIR, "bash-laplacian.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-laplacian.mha"), os.path.join(RESDIR, "bash-laplacian.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": laplacian, comparison with bash result is " + str(c))
