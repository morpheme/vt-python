#!/bin/bash

echo "*** Linear filtering "
./linear_filter.sh
python3.7 linear_filter_filename.py 
python3.7 linear_filter_structure.py

echo "*** Cell-based mathematical morphology"
./cellfilter.sh
python3.7 cellfilter_filename.py
python3.7 cellfilter_filename.py

echo "*** Image registration, image resampling, transformation composition"
./blockmatching.sh
python3.7 blockmatching_filename.py 
python3.7 blockmatching_structure.py

echo "*** Point list registration, image resampling, point resampling"
./pointmatching.sh
python3.7 pointmatching_filename.py 
python3.7 pointmatching_structure.py
