#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
Usage example for data structure `vtImage`.
"""

from vt import vtImage
from vt.dataset import shared_data_path
from vt.tools import random_array

# --- Initialize an EMPTY vtImage:
vt_img = vtImage()
print(vt_img)
print(vt_img.spacing())
print(vt_img.shape())
# - change spacing:
vt_img.setShape([5, 10, 15])
print(vt_img.shape())
# - change spacing:
vt_img.setSpacing([0.1, 0.2, 0.3])
print(vt_img.spacing())

# --- Creation from a random 2D NumPy array:
arr = random_array((4, 5), dtype='uint8')
print(arr.shape)
print(arr.dtype)

vt_img = vtImage(arr, [0.2, 0.3, 1.0])
print(vt_img.spacing())
print(vt_img.shape())

vt_arr = vt_img.copy_to_array()
print(vt_arr.shape)
print(vt_arr.dtype)

# --- Creation from a random 3D NumPy array:
arr = random_array((3, 4, 5), dtype='uint8')
print(arr.shape)
print(arr.dtype)

vt_img = vtImage(arr, [0.2, 0.3, 0.1])
print(vt_img.spacing())
print(vt_img.shape())

vt_arr = vt_img.copy_to_array()
print(vt_arr.shape)
print(vt_arr.dtype)

 # - Creation from a saved 3D image:
vt_img = vtImage(shared_data_path('flower_confocal', 0))
print(vt_img.spacing())
print(vt_img.shape())
