import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-cellfilter"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)

#
# example with file names
#

vt.cellfilter(os.path.join(DATADIR, "p58-t0_SEG_down_interp_2x.inr"), "-operation opening -R 2 -no-verbose",
                 out_file_path=os.path.join(RESDIR, "py-filename-cellfilter.mha"))

if os.path.isfile(os.path.join(RESDIR, "bash-cellfilter.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-filename-cellfilter.mha"), os.path.join(RESDIR, "bash-cellfilter.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": comparison with bash result is " + str(c))
