#!/bin/bash

DATADIR=../data
RESDIR=result-blockmatching

mkdir -p ${RESDIR}

#
# 
#

blockmatching -flo ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	      -ref ${DATADIR}/p58-t1_INT_down_interp_2x.inr \
	      -res-trsf ${RESDIR}/bash-transfo-affine.trsf \
	      -trsf-type affine -py-ll 2 \
	      -res ${RESDIR}/bash-floating-after-affine-tmp.mha

applyTrsf ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	  ${RESDIR}/bash-floating-after-affine.mha \
	  -ref ${DATADIR}/p58-t1_INT_down_interp_2x.inr \
	  -trsf ${RESDIR}/bash-transfo-affine.trsf

blockmatching -flo ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	      -ref ${DATADIR}/p58-t1_INT_down_interp_2x.inr \
	      -left-trsf ${RESDIR}/bash-transfo-affine.trsf \
	      -res-trsf ${RESDIR}/bash-transfo-deform.trsf \
	      -trsf-type vectorfield -py-ll 2 \
	      -res ${RESDIR}/bash-floating-after-composition-tmp.mha

composeTrsf -trsfs ${RESDIR}/bash-transfo-affine.trsf \
	    ${RESDIR}/bash-transfo-deform.trsf \
	    -res ${RESDIR}/bash-transfo-composition.trsf

applyTrsf ${DATADIR}/p58-t0_INT_down_interp_2x.inr \
	  ${RESDIR}/bash-floating-after-composition.mha \
	  -ref ${DATADIR}/p58-t1_INT_down_interp_2x.inr \
	  -trsf ${RESDIR}/bash-transfo-composition.trsf
