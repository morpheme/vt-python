#!/bin/bash

DATADIR=../data
RESDIR=result-pointmatching

mkdir -p ${RESDIR}


#
# pointmatching computes a transformation that allows to resample
# the floating image from which the floating points are drawn
# -> Thus it goes from the reference image to the floating ones
# -> Thus we can transform the reference points
#

pointmatching -reference $DATADIR/cell_barycenters_landmarks-t1.txt \
	      -floating $DATADIR/cell_barycenters_landmarks-t0.txt \
	      -res-trsf $RESDIR/bash-trsf-rigid-t1-t0.trsf \
	      -trsf-type rigid -estimator-type lts

applyTrsf -reference $DATADIR/p58-t1_INT_down_interp_2x.inr \
	  -floating $DATADIR/p58-t0_INT_down_interp_2x.inr \
	  -trsf $RESDIR/bash-trsf-rigid-t1-t0.trsf \
	  -result $RESDIR/bash-p58-t0-in-t1-rigid_INT_down_interp_2x.mha

applyTrsfToPoints $DATADIR/cell_barycenters_landmarks-t1.txt \
		  $RESDIR/bash-cell_barycenters_landmarks-t1-rigid.txt \
		  -trsf $RESDIR/bash-trsf-rigid-t1-t0.trsf

pointmatching -reference $DATADIR/cell_barycenters_landmarks-t1.txt \
	      -floating $DATADIR/cell_barycenters_landmarks-t0.txt \
	      -template $DATADIR/p58-t1_INT_down_interp_2x.inr \
	      -res-trsf $RESDIR/bash-trsf-vectorfield-t1-t0.trsf \
	      -trsf-type vectorfield \
	      -vector-propagation-type skiz \
	      -vector-fading-distance 15 \
	      -vector-propagation-distance 20 \
	      -estimator-type lts \
	      -lts-iterations 5

applyTrsf -reference $DATADIR/p58-t1_INT_down_interp_2x.inr \
	  -floating $DATADIR/p58-t0_INT_down_interp_2x.inr \
	  -trsf $RESDIR/bash-trsf-vectorfield-t1-t0.trsf \
	  -result $RESDIR/bash-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha

applyTrsfToPoints $DATADIR/cell_barycenters_landmarks-t1.txt \
		  $RESDIR/bash-cell_barycenters_landmarks-t1-vectorfield.txt \
		  -trsf $RESDIR/bash-trsf-vectorfield-t1-t0.trsf
