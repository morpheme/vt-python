import definitions

definitions.add_vt_path()

import vt
import os
import filecmp

DATADIR = "../data"
RESDIR = "result-pointmatching"

if not os.path.isdir(RESDIR):
    os.makedirs(RESDIR)
    
#
# pointmatching computes a transformation that allows to resample
# the floating image from which the floating points are drawn
# -> Thus it goes from the reference image to the floating ones
# -> Thus we can transform the reference points
#

#
# example with python structures
#

points_flo = vt.vtPointList(os.path.join(DATADIR, "cell_barycenters_landmarks-t0.txt"))
points_ref = vt.vtPointList(os.path.join(DATADIR, "cell_barycenters_landmarks-t1.txt"))



image_flo = vt.vtImage(os.path.join(DATADIR, "p58-t0_INT_down_interp_2x.inr"))
image_ref = vt.vtImage(os.path.join(DATADIR, "p58-t1_INT_down_interp_2x.inr"))

#
#
#

trsf_rigid = vt.pointmatching(points_flo, points_ref, params="-trsf-type rigid -estimator-type lts" )

#
# we write and re-read the transformation to have the same float truncature
# else all tests (apart the first one) will be False
#
# THIS IS ONLY FOR TEST PURPOSE!
#
trsf_rigid.write(os.path.join(RESDIR, "py-structure-trsf-rigid-t1-t0.trsf"))
del trsf_rigid
trsf_rigid = vt.vtTransformation(os.path.join(RESDIR, "py-structure-trsf-rigid-t1-t0.trsf"))

image_rigid = vt.apply_trsf(image_flo, trsf_rigid, ref=image_ref)
image_rigid.write(os.path.join(RESDIR, "py-structure-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"))

points_rigid = vt.apply_trsf_to_points(points_ref, trsf_rigid)
points_rigid.write(os.path.join(RESDIR, "py-structure-cell_barycenters_landmarks-t1-rigid.txt"))

#
#
#
params = "-trsf-type vectorfield -vector-propagation-type skiz -vector-fading-distance 15"
params += " -vector-propagation-distance 20 -estimator-type lts -lts-iterations 5"

trsf_deform = vt.pointmatching(points_flo, points_ref, ref=image_ref, params=params)
trsf_deform.write(os.path.join(RESDIR, "py-structure-trsf-vectorfield-t1-t0.trsf"))
del trsf_deform
trsf_deform = vt.vtTransformation(os.path.join(RESDIR, "py-structure-trsf-vectorfield-t1-t0.trsf"))

image_deform = vt.apply_trsf(image_flo, trsf_deform, ref=image_ref)
image_deform.write(os.path.join(RESDIR, "py-structure-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"))

points_deform = vt.apply_trsf_to_points(points_ref, trsf_deform)
points_deform.write(os.path.join(RESDIR, "py-structure-cell_barycenters_landmarks-t1-vectorfield.txt"))

#
#
#

if os.path.isfile(os.path.join(RESDIR, "bash-trsf-rigid-t1-t0.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-trsf-rigid-t1-t0.trsf"),
                    os.path.join(RESDIR, "bash-trsf-rigid-t1-t0.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": rigid transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-p58-t0-in-t1-rigid_INT_down_interp_2x.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"),
                    os.path.join(RESDIR, "bash-p58-t0-in-t1-rigid_INT_down_interp_2x.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after linear transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-rigid.txt")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-cell_barycenters_landmarks-t1-rigid.txt"),
                    os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-rigid.txt"),
                    shallow=False)
    print(" - " + str(__file__) + ": points after linear transformation, comparison with bash result is " + str(c))
    
if os.path.isfile(os.path.join(RESDIR, "bash-trsf-vectorfield-t1-t0.trsf")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-trsf-vectorfield-t1-t0.trsf"),
                    os.path.join(RESDIR, "bash-trsf-vectorfield-t1-t0.trsf"),
                    shallow=False)
    print(" - " + str(__file__) + ": vectorfield transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"),
                    os.path.join(RESDIR, "bash-p58-t0-in-t1-vectorfield_INT_down_interp_2x.mha"),
                    shallow=False)
    print(" - " + str(__file__) + ": image after vectorfield transformation, comparison with bash result is " + str(c))

if os.path.isfile(os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-vectorfield.txt")):
    c = filecmp.cmp(os.path.join(RESDIR, "py-structure-cell_barycenters_landmarks-t1-vectorfield.txt"),
                    os.path.join(RESDIR, "bash-cell_barycenters_landmarks-t1-vectorfield.txt"),
                    shallow=False)
    print(" - " + str(__file__) + ": points after vectorfield transformation, comparison with bash result is " + str(c))
